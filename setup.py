# This file needs to be run with arguments as
# python3 setup.py sdist bdist_wheel --formats=zip

import setuptools
import os
import sys

ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

with open(f'{ROOT_PATH}/README.md', 'r') as file:
    long_description = file.read()

project_name = "cdtipy"
author_list = ["Jonathan Weine", ]
author_email_list = ["weine@biomed.ee.ethz.ch", ]
url = "https://git.ee.ethz.ch/jweine/cdtipy"

# Get version tag
if '--version' in sys.argv:
    tag_index = sys.argv.index('--version') + 1
    current_version = sys.argv[tag_index]
    sys.argv.pop(tag_index-1)
    sys.argv.pop(tag_index-1)
else:
    raise ValueError('No version as keyword "--version" was specified')

with open('cdtipy/__init__.py', 'a') as module_header_file:
    module_header_file.write(f'\n__version__ = "{current_version}"')

setuptools.setup(
    name=project_name,
    url=url,
    version=current_version,
    author=author_list,
    author_email=author_email_list,
    long_description=long_description,
    include_package_data=True,
    packages=setuptools.find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    python_requires=">=3.6",
    install_requires=["tensorflow", "tqdm", "numpy", "scipy", "matplotlib",
                      "jupyterlab", "opencv-python", "ipympl", "seaborn", "ipywidgets",
                      "parrec-reader-py", "scikit-image"]
)
