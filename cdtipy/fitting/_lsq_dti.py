__all__ = ["LSQLinear", "MatrixInversion"]

from typing import *

import tqdm
import numpy as np
from scipy.optimize import Bounds, minimize, lsq_linear
from scipy.linalg import pinvh

from ._lsq_base import TensorRegressor

ConstraintTuple = Tuple[float, float, float, float, float, float, float]  # (1 + 6) free parameters for fitting


class MatrixInversion(TensorRegressor):
    def fit(self, disable_progressbar: bool = False):
        """
        .. math::
            \hat{D} = (log(S0), D_xx, D_yy, D_zz, D_xy, D_xz, D_yz).T

            B (N, 7) = (-1, bx ** 2, by ** 2, bz ** 2, 2 * bx * by, 2 * bx * bz, 2 * by * bz) for each dir

            log(S(b)) = - B @ \hat{D}

             - B{^-1} @ log(S(b)) = B^{-1} @ B @ \hat{D}

        :param disable_progressbar:
        :return:
        """
        number_of_pixels = self.masked_data.shape[0]
        bx, by, bz = self.b_vectors.T
        s0_const = np.ones_like(bx)
        flat_extended_b_tensors = -np.array([-s0_const, bx ** 2, by ** 2, bz ** 2,
                                             2 * bx * by, 2 * bx * bz, 2 * by * bz]).T
        inverse = np.linalg.pinv(flat_extended_b_tensors)
        for pixel_index in tqdm.tqdm(range(number_of_pixels), disable=disable_progressbar):
            pixel_signal = self.masked_data[pixel_index, :, :].flatten()
            cleaned_signal = np.maximum(np.zeros_like(pixel_signal) + 1e-10, pixel_signal)
            y = np.log(cleaned_signal)
            y = np.where(np.isfinite(y), y, np.zeros_like(y))
            res = inverse @ y
            self._s0[pixel_index] = np.exp(res[0])
            self._d_diff[pixel_index] = res[1:]


class LSQLinear(TensorRegressor):
    def fit(self, contraints: Optional[Tuple[Iterable[float], Iterable[float]]] = None,
            disable_progressbar: bool = False):
        """ Solves the linear lsq while clipping negative magnitude values (due to noise) to 0.

        :param contraints:
        :param disable_progressbar:
        :return:
        """
        number_of_pixels = self.masked_data.shape[0]
        if contraints is None:
            contraints = (-np.inf, np.inf)

        bx, by, bz = self.b_vectors.T
        s0_const = np.ones_like(bx)
        flat_extended_b_tensors = -np.array([-s0_const, bx ** 2, by ** 2, bz ** 2, 2 * bx * by, 2 * bx * bz, 2 * by * bz]).T

        for pixel_index in tqdm.tqdm(range(number_of_pixels), disable=disable_progressbar):
            pixel_signal = self.masked_data[pixel_index, :, :].flatten()
            cleaned_signal = np.maximum(np.zeros_like(pixel_signal), pixel_signal)
            y = np.log(cleaned_signal)
            y = np.where(np.isfinite(y), y, np.zeros_like(y))
            res = lsq_linear(A=flat_extended_b_tensors, b=y, bounds=contraints)
            optimal_parameters: np.ndarray = res.x  # noqa
            self._s0[pixel_index] = np.exp(optimal_parameters[0])
            self._d_diff[pixel_index] = optimal_parameters[1:]


class LSQDerivativeFree(TensorRegressor):

    def fit(self, constraints: Optional[ConstraintTuple] = None) -> None:
        global_parameter_bounds = None
        if constraints is not None:
            lo, up = np.array([[-j, i] for j, i in zip([-0., -0., -0., -0., *constraints[4:]], constraints)]).T
            print(lo, up)
            global_parameter_bounds = Bounds(lo, up)

        number_of_pixels = self.masked_data.shape[0]
        optimal_parameters_array = np.zeros((number_of_pixels, 7), np.float64)

        # Loop over pixels and fit every curve separately
        for pixel_index in tqdm.tqdm(range(number_of_pixels)):
            y = self.masked_data[pixel_index, :, :].flatten()
            initial_parameters = np.array((self._s0[pixel_index], *self._d_diff[pixel_index, :]), np.float64)  # (7, )
            mse_function = self._get_optimization_function(y, self.b_vectors)

            res = minimize(mse_function, initial_parameters, method='trust-constr', bounds=global_parameter_bounds)
            optimal_parameters_array[pixel_index, :] = res.x

        self._s0[:] = optimal_parameters_array[:, 0:1]
        self._d_diff[:, :] = optimal_parameters_array[:, 1:]

    @staticmethod
    def _get_optimization_function(data: np.ndarray, b_vectors: np.ndarray):
        """ Factory function to return callable error function which is passed to the optimization process
        :param data: (-1, )
        :param b_vectors: (-1, 3)
        """
        bx, by, bz = b_vectors.T
        flat_b_tensors = np.array([bx ** 2, by ** 2, bz ** 2, 2 * bx * by, 2 * bx * bz, 2 * by * bz])

        def mse_err(params):
            s0 = params[0:1].T
            ddiff = params[1:7].T

            exponent = - np.einsum('in, i -> n', flat_b_tensors, ddiff)
            prediction = s0 * np.exp(exponent)
            squared_error = np.power(data - prediction, 2)
            mse = np.mean(squared_error)
            return mse
        return mse_err
