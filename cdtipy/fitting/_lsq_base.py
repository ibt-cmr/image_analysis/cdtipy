__all__ = ["TensorRegressor"]

from typing import *
from abc import abstractmethod
import warnings
import numpy as np


class TensorRegressor(object):
    mask: np.ndarray = None
    magnitude_data: np.ndarray = None
    b_values: np.ndarray = None
    directions: np.ndarray = None
    _s0, _f, _d_diff, _d_perf = None, None, None, None
    masked_data = None

    def __init__(self, data: np.ndarray, b_vectors: np.ndarray,  mask: Optional[np.ndarray] = None,
                 initial_values: Optional[Tuple[np.ndarray, np.ndarray]] = None, **kwargs):
        """

        :param data: (N, M, #bvecs, #agv)
        :param b_vectors: (# b_vecs, 3) assumes that <b_vec, b_vec> = b  (sqrt-scaling)
        :param mask: (M, N)
        :param initial_values: (len(mask[ > 0].flatten()), #dir) per value, is assigned to _s0, _d_diff
        :param kwargs:
        """
        if b_vectors.shape[0] != data.shape[2]:
            raise ValueError(f"Number of b-values {b_vectors.shape} does not match data dimensions {data.shape}")
        self.b_vectors = b_vectors

        assert len(data.shape) == 4
        self.magnitude_data = np.abs(data)

        if mask is None:
            image_dimensions = data.shape[0:2]
            self.mask = np.ones(image_dimensions, np.int64)
        else:
            self.mask = mask.astype(np.int64)
        self._mask_indices = np.where(self.mask > 0.1)

        number_of_pixels_in_mask = len(self._mask_indices[0])
        self.masked_data = np.zeros((number_of_pixels_in_mask, data.shape[2], data.shape[3]))
        self.masked_data[:, :, :] = self.magnitude_data[self._mask_indices[0], self._mask_indices[1], :, :]

        if initial_values is None:
            self._s0 = np.ones((number_of_pixels_in_mask, 1), np.float64)
            self._d_diff = np.tile(np.array([1E-3, 3E-4, 3E-4, 0., 0., 0.]), [number_of_pixels_in_mask, 1])
        else:
            self._s0, self._d_diff = initial_values
            self._s0 = self._s0.reshape(-1, 1)

        for key, value in kwargs.items():
            try:
                self.__dict__[key] = value
            except KeyError:
                warnings.warn(f"Tried to pass unexpected keyword argument: {key}")

    def predict(self, b_vectors: np.ndarray):
        """ Predicts the signal at specified b-values by evaluating the tensor signal model with the fitted parameters:
        .. math::
            S(b) = S0 e^{- b g^T D g}

        :param b_vectors: (#b_vecs, [#avg], 3) assumes that <b_vec[i, ..., :], b_vec[i, ..., :]> = b  (sqrt-scaling)
        :return: (#pixels, #b_vectors, [#avg])
        """
        if len(b_vectors.shape) > 2:
            number_of_averages = b_vectors.shape[1]
            n_distinct_vectors = b_vectors.shape[0]
            b_vectors = b_vectors.reshape((-1, 3))
        else:
            number_of_averages = 1
            n_distinct_vectors = b_vectors.shape[0]

        s0 = self._s0.flatten()
        bx, by, bz = b_vectors.T
        flat_b_tensors = np.array([bx**2, by**2, bz**2, 2*bx*by, 2*bx*bz, 2*by*bz])
        exponent = - np.einsum('in, pi -> np', flat_b_tensors, self._d_diff)
        signal = np.einsum('p, np -> np', s0, np.exp(exponent))
        return np.transpose(signal.reshape((n_distinct_vectors, number_of_averages, -1)), [2, 0, 1])

    @abstractmethod
    def fit(self):
        return None

    def get_result(self, dimensions: Optional[int] = 1):
        """ Returns fitted tensors and s0 either as flat array of pixels or 2D map
        :param dimensions: int
        :return: (np.ndarray, np.ndarray)
        """
        if dimensions == 1:
            result_array1 = self._s0
            result_array2 = self._d_diff

        elif dimensions == 2:
            x_dim, y_dim = self.magnitude_data.shape[0:2]

            result_array1 = np.zeros((x_dim, y_dim), np.float64)
            result_array1[self._mask_indices[0], self._mask_indices[1]] = self._s0[:, 0]

            result_array2 = np.zeros((x_dim, y_dim, 3, 3), np.float64)
            tensor_arr = self._flat_tensor_to_matrix(self._d_diff)
            result_array2[self._mask_indices[0], self._mask_indices[1], :, :] = tensor_arr

        else:
            result_array1, result_array2 = None, None
        return result_array1, result_array2

    @staticmethod
    def _flat_tensor_to_matrix(flat_tensor_array: np.ndarray):
        """ Transforms diffusion tensor from [Dxx, Dyy, Dzz, Dxy, Dxz, Dyz] to stack of 2D matrices with shape [N, 3, 3]
        :param flat_tensor_array:
        :return: (np.ndarray) [N, 3, 3]
        """
        number_of_pixels = len(flat_tensor_array)
        tensor = np.zeros((number_of_pixels, 3, 3), np.float64)
        for i in range(number_of_pixels):
            (dxx, dyy, dzz, dxy, dxz, dyz) = flat_tensor_array[i, :]
            tensor[i, :, 0] = [dxx, dxy, dxz]
            tensor[i, :, 1] = [dxy, dyy, dyz]
            tensor[i, :, 2] = [dxz, dyz, dzz]
        return tensor
