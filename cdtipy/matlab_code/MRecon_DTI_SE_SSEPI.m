classdef MRecon_DTI_SE_SSEPI < MRecon
    % MRecon_DTI_SE_SSEPI
    % MRecon subclass to reconstruct cardiac diffusion tensor imaging
    % data acquired with single-shot SE.
    % 
    % Assumes: 
    %     - b-value indices [0, 1, 2] in Parameter.Parameter2Read.extr1
    %            following a b0, b100, b450 scheme
    %     - diffusion directions: [0:12] in Parameter.Parameter2Read.extr2
    %            following a b0, 3xlow, 9xhigh scheme              
    %     - call to perform requires a call to load_sense(ref_path) beforehand
    %
    %  Reconstructed images:
    %      - Access the MR.Data field after calling Perform
    %      - Returned shape will be (X, Y, #slice, #directions, #avg)
    
    properties
    end
    
    methods
        function MR = MRecon_DTI_SE_SSEPI( filename )
            if nargin == 0
                filename = '';
            end
            MR = MR@MRecon(filename);
            
        end
        function perform_type1(MR, diffusion_dir_idx, b_value_idx)
            % perform_type1 - Evaluates the reconstruction for type 1 data
            %      which means it is within the respiratory gating window
            %
            % Arguments:
            %     - diffusion_dir_idx:
            %       Index of diffusion encoding direction corresponding to
            %       the acquired order. In the easiest case it is equal to
            %       0:MR.Parameter.Labels.GradientOris-1
            %     - b_value_idx:
            %       Index of diffusion encoding strength corresponding to
            %       the acquired order. In the easiest case it is equel to
            %       b_value_idx = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3] - 1
            %
           
            MR.Parameter.Parameter2Read.typ = 1;
            MR.Parameter.Parameter2Read.Update;
            MR.Parameter.Recon.AutoUpdateInfoPars = 'no';
            MR.Parameter.Recon.Average = 'No';
            MR.Parameter.Recon.RingingFilterStrength = [0.25, 0.25, 0.25];

            % Accumulate every diffusion direction in cell array that will be stacked 
            % and assigned to MR.data at the end of Perform
            resulting_data = {};
            n_steps = numel(diffusion_dir_idx);
            for temp = [1:n_steps; diffusion_dir_idx; b_value_idx]
                % Set Labels for Reading the data according to direction/bval

                MR.Parameter.Parameter2Read.extr1 = temp(3);
                MR.Parameter.Parameter2Read.extr2 = temp(2);

                MR.mrperform;         
                resulting_data{end+1} = MR.Data;

                % Display the progress
                percentDone = 100 * temp(1) / n_steps;
                msg = strcat(sprintf('Recon status: %3.1f', percentDone), '%');
                disp(msg)
            end

            % Concatenate data along the directions-axis and squeeze singleton dimensions
            MR.Data = squeeze(cat(13, resulting_data{:}));
            MR.Parameter.Reset;
        end

        function save_indices = perform_type2(MR, diffusion_dir_idx, b_value_idx)
            % perform_type2 - Evaluates the reconstruction for type 2 data
            %       which means it rejected by the respiratory gating window
            %
            % Arguments:
            %     - diffusion_dir_idx:
            %       Index of diffusion encoding direction corresponding to
            %       the acquired order. In the easiest case it is equal to
            %       0:MR.Parameter.Labels.GradientOris-1
            %     - b_value_idx:
            %       Index of diffusion encoding strength corresponding to
            %       the acquired order. In the easiest case it is equel to
            %       b_value_idx = [1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3] - 1
            %

            MR.Parameter.Parameter2Read.typ = 2;
            MR.Parameter.Parameter2Read.Update;
            MR.Parameter.Recon.AutoUpdateInfoPars = 'yes';
            MR.Parameter.Recon.Average = 'No';
            MR.Parameter.Recon.RingingFilterStrength = [0.25, 0.25, 0.25];

            % Accumulate every diffusion direction in cell array that will be stacked
            % and assigned to MR.data at the end of Perform
            resulting_data = {};
            b_vec_indices = {};

            for extr_indices = [diffusion_dir_idx; b_value_idx]
                % Set Labels for Reading the data according to direction/bval
                MR.Parameter.Parameter2Read.extr1 = extr_indices(2);
                MR.Parameter.Parameter2Read.extr2 = extr_indices(1);

                % --------------------------------------------------------
                % Perform the Reconstruction for the Current Chunk (Start)
                % Contains all MRecon specific calls
                % --------------------------------------------------------
                try
                    MR.mrperform;
                    size(MR.Data)
                    data = squeeze(MR.Data);
                    dat_size = size(data);
                    resulting_data{end+1} = reshape(data, dat_size(1), dat_size(2), []);
                    b_vec_indices(end+1) = {repmat([extr_indices(1)  extr_indices(2)], ...
                                                                  [dat_size(3)*dat_size(4), 1])};
                catch
                    warning('Perform failed')
                end

            end

            temp_data = cat(3, resulting_data{:});
            temp_indices = cat(1, b_vec_indices{:});
            total_imgs = size(temp_data)

            return_data = {};
            return_indices = {};
            for i = 1:total_imgs(3)
                sum(abs(temp_data(:, :, i)), 'all')
                if sum(abs(temp_data(:, :, i)), 'all') > 0
                    return_data{end+1} = temp_data(:, :, i);
                    return_indices{end+1} = temp_indices(i, :);
                end
            end

            MR.Data = cat(3, return_data{:});
            MR.Parameter.Reset;
            save_indices = cat(1, return_indices{:});
        end

        function load_sense(MR, ref_scan_path)
            % Load sensitivities for reconstruction 
            S = MRsense(ref_scan_path,MR.Parameter.Filename.Data);
            S.MatchTargetSize = 1; 
            S.Perform;
            MR.Parameter.Recon.Sensitivities = S;
        end

        function mrperform(MR)
            %%% Actual MRecon calls for a chunk of data
            MR.Parameter.Recon.Average = 'No';
            MR.Parameter.Recon.ImmediateAveraging = 'No';
            MR.ReadData;

            MR.Parameter.Recon.RingingFilterStrength=[0.15,0.15,0.15];
            MR.Parameter.Recon.SENSERegStrength = 0;
            MR.BasicCorrections;
            MR.SortData;
            MR.GridData;
            MR.RingingFilter;
            MR.ZeroFill;
            MR.K2IM;
            MR.EPIPhaseCorrection;
            MR.K2IP;
            MR.GridderNormalization;
            MR.SENSEUnfold;
            MR.RemoveOversampling;
            MR.CombineCoils;
            MR.GeometryCorrection;
        end
    end
end


