function result_filename =  call_recon(ref_scan_path, data_file_path, save_location, ...
                                       diffusion_dir_idx, b_value_idx, reconstruction_type)
    % Calls DTI_SE_SSEPI reconstruction and saves result to mat files
    
    MR = MRecon_DTI_SE_SSEPI(data_file_path);
    MR.load_sense(ref_scan_path);

    if reconstruction_type == 1.
        MR.perform_type1(diffusion_dir_idx, b_value_idx);
        result_filename = fullfile(save_location, 'non_averaged_recon_result.mat');
    elseif reconstruction_type == 2.
        save_indices = MR.perform_type2(diffusion_dir_idx, b_value_idx);
        result_filename = fullfile(save_location, 'non_averaged_recon_result_type2.mat');
        save(fullfile(save_location, 'type2_indices.mat'), 'save_indices');
    else
        ME = MException('Invalid reconstruction_type. Given value (%d) Not is [1, 2]', ...
                        reconstruction_type);
        throw(ME)
    end

    data_real = real(MR.Data);
    data_imag = imag(MR.Data);
%    parameters = struct(MR.Parameter);
%    save(result_filename, 'data_real', 'data_imag', 'parameters');
    save(result_filename, 'data_real', 'data_imag');
end