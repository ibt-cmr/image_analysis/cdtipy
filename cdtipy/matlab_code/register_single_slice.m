function [imdef, map_to_first] = register_single_slice(imgs0, mask)
    % Perfoms ptv registration on Diffusion weighted images and applies
    % image-deformation to all input images according to the obtained
    % displacementfields, such that heart appears at location of the first
    % image.
    %
    % NOTE: Registration will not work if the unweighted b0 image is included!
    % 
    % Input:
    %       - image_stack: complex array of shape (X, Y, -1, -1)
    %       - mask: double array (X, Y)
    %
    % Returns:
    %       - image
    %       - displacements
    % 
    % reshape and normalized images -> all directions/avs concatenated at last
    % axis, and per diffusion_weighting scaled to range (0, 1)
    sz0 = size(imgs0);

	img = abs(imgs0) ./ (max(abs(imgs0), [], [1,2]) + 1e-6);
	img = sqrt(img);
	img = img ./ (max(abs(img), [], [1,2]) + 1e-6);
	img = reshape(img, size(img,1), size(img,2), 1, 1, []);
    
    % Set registration options as specified in ptv_register
    opts = [];
    opts.pix_resolution = [1,1];
    opts.metric = 'local_nuclear';
	opts.local_nuclear_patch_size = 20;
	
    opts.grid_spacing = [10, 10];
    opts.spline_order = 1;
    opts.interp_type = 1;
    opts.k_down = 0.75;
    opts.display = 'off';
    opts.max_iters = [150, 150, 150, 150];
	opts.mean_penalty = 0.0001;
	
    opts.fixed_mask = double(mask);
    opts.border_mask = 5;
    
    opts.singular_coefs = ones(size(img, 5), 1);
    opts.singular_coefs(1) = 0;
	opts.isoTV = 0.04;    
	
    tic;
    [~, warp_to_original, ~] = ptv_register(img, [], opts);
    toc

    [map_to_first, ~] = remap_displacements(warp_to_original, 1, img, opts.pix_resolution);
    
    
    img0_flat = reshape(imgs0, size(img,1), size(img,2), 1, 1, []);
    imdef_real = ptv_deform(real(img0_flat), map_to_first, 1);
    imdef_imag = ptv_deform(imag(img0_flat), map_to_first, 1);
    imdef = complex(imdef_real, imdef_imag);
    imdef = reshape(imdef, sz0); 
 end
