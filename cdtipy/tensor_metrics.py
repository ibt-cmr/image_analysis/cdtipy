__all__ = ['eigen_decomposition', 'fractional_anisotropy', 'relative_anisotropy', 'dense_fa']

from typing import Union
import tensorflow as tf
import numpy as np

import cdtipy.utils


def metric_set(tensors, flat_tensors: bool = False):
    """ Computes the eigen-decomposition and a set of
    common diffusion tensor metrics for of a batch of
    tensors.

    :param tensors: tf.Tensor or np.ndarray containing the
                tensors either as map or flat vector.
    :param flat_tensors: bool - indicates if tensors are
    given in ...,3,3 or ...,6 shape
    :return: dict('eval', 'evecs', 'MD', 'FA', 'RA')
    """
    if len(tensors.shape) == 1 + 2 - int(flat_tensors):
        tensors = tensors[tf.newaxis, ...]

    if flat_tensors:
        tensors = cdtipy.utils.flat_tensor_to_matrix(tensors)

    evals, evecs = eigen_decomposition(tensors)
    fa = fractional_anisotropy(evals)
    md = mean_diffusivity(tensors)
    ra = relative_anisotropy(evals)
    return {'evals': evals, 'evecs': evecs, 'MD': np.array(md), 'FA': np.array(fa), 'RA': ra}


def eigen_decomposition(tensor_map):
    """ Returns the eigen decomposition of the square diffusion tensors.
    The oder of the returned vectors and values correspond to descending
    eigenvalues where the innermost axis indexes the different eigenvectors.

    .. math::
        eval[..., i] \cdot evec[..., :, i] = D[..., :, :] @ evec[..., :, i]

    :param tensor_map: (X, Y, 3, 3)
    :return: eigen
    """
    evals, evecs = np.linalg.eig(tensor_map)
    flip_sign_val = np.sign(evals)
    evecs = evecs * flip_sign_val[..., np.newaxis, :]
    evals = evals * flip_sign_val

    indices = np.argsort(evals)[..., ::-1]
    sorted_vec = np.zeros_like(evecs)
    sorted_val = np.zeros_like(evals)
    for i in range(evecs.shape[0]):
        for j in range(evecs.shape[1]):
            for k_idx, k in enumerate(indices[i, j]):
                sorted_vec[i, j, :, k_idx] = evecs[i, j, :, k]
                sorted_val[i, j, k_idx] = evals[i, j, k]
    return sorted_val, sorted_vec


def mean_diffusivity(tensors: tf.Tensor) -> tf.Tensor:
    """ Calculates mean diffusivity

    :param tensors: (..., 3, 3)
    :return:
    """
    return tf.einsum('...ii', tensors) / 3


def fractional_anisotropy(tensor_eigenvalues: Union[np.ndarray, tf.Tensor]) -> tf.Tensor:
    """Calculates fractional anisotropy batch-wise. 
    Assumes the input eigen values to be sorted in descending order
    
    :param tensor_eigenvalues: (..., 3)
    :return: fa (...) tf.Tensor
    """
    ev1, ev2, ev3 = [tensor_eigenvalues[..., i] for i in range(3)]
    mean_diffusivity = (ev1 + ev2 + ev3) / 3.
    term1 = (ev1 - mean_diffusivity) ** 2 + (ev2 - mean_diffusivity) ** 2 + (ev3 - mean_diffusivity) ** 2
    term2 = (ev1 ** 2 + ev2 ** 2 + ev3 ** 2)
    fa = tf.sqrt(3./2. * tf.math.divide_no_nan(term1, term2))
    return fa


def dense_fa(tensor_map):
    mask = tf.einsum('xyii', tensor_map) > 0
    flat_tensors = tf.gather_nd(tensor_map, tf.where(mask))
    evs = tf.linalg.eigvalsh(flat_tensors)
    fa = fractional_anisotropy(evs)
    return cdtipy.utils.fill_dense_map(mask, fa)


def relative_anisotropy(tensor_eigenvalues: Union[np.ndarray, tf.Tensor]):
    """Calculates relative anisotropy batch-wise. 
    Assumes the input eigen values to be sorted in descending order
    
    :param tensor_eigenvalues: (..., 3)
    """
    ev1, ev2, ev3 = [tensor_eigenvalues[..., i] for i in range(3)]
    term1 = (ev1 - ev2) ** 2 + (ev2 - ev3) **2 + (ev3 - ev2) **2
    term2 = (ev1 + ev2 + ev3)
    ra_map = tf.sqrt(1./2. * term1 / (term2 + 1e-7))
    return np.nan_to_num(ra_map)

