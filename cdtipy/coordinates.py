__all__ = ["get_relative_polar_coordinates", "get_mask_centered_coordinates", "compute_transmural_position",
           "compute_local_basis_2d"]

from typing import Optional, Union, Tuple
import tensorflow as tf
import numpy as np
import cv2

from cdtipy.utils import fill_dense_map


def get_relative_polar_coordinates(mask: Union[np.ndarray, tf.Tensor], fov: Union[np.ndarray, tf.Tensor],
                                   offset_angle: float = 0., inner_border_offset=1.,
                                   fill_background=np.nan) -> tf.Tensor:
    """ Comuputes the transmural depth and angle coordinates inside the LV mask.

    :param mask:
    :param fov:
    :param offset_angle: in degree - Angle added to define the zero-value of the angular coordinate.
                         By default it is 0 degree and the range is from -180 to 180.
    :param inner_border_offset: is necessary as the transmural coordinate map is set to -1. outside of the mask
    :param fill_background: value with wich the backgorund of is filled
    :return: (X, Y, 2)
    """
    mask = tf.cast(mask, tf.float32)
    polar_coordinates, cartesian_coordinates = get_mask_centered_coordinates(mask, fov)

    # The addition of 1.1 is meant to deal with the -1 assignment at the borders of mask
    transmural_depth_map = compute_transmural_position(cartesian_coordinates, mask) + inner_border_offset

    if offset_angle != 0.:
        angle = np.mod(polar_coordinates[..., 1] + np.pi + np.deg2rad(offset_angle), 2*np.pi) - np.pi
    else:
        angle = polar_coordinates[..., 1]

    polar_coordinates = tf.stack((transmural_depth_map, angle), axis=2)
    mask = tf.where(transmural_depth_map >= 0, tf.ones_like(transmural_depth_map), tf.zeros_like(transmural_depth_map))
    flat_coordinates = tf.gather_nd(polar_coordinates, tf.where(mask > 0.5))
    polar_coordinates = fill_dense_map(mask, flat_coordinates, default_fill_value=fill_background)
    return polar_coordinates


def get_mask_centered_coordinates(mask: np.ndarray, fov: np.ndarray) -> (tf.Tensor, tf.Tensor):
    """ Takes fov-dimensions and a myocard_mask to calculated the coordinate system centered on the
    center of mass of the LV. Returns gridded cartesian as well as polar coordinates.

    :param mask: (X, Y)
    :param fov: (2, )
    :return: polar coordinates (m, radians), cartesian coordinates (m, m)
    """
    # fov = np.array(fov, dtype=np.float32)
    nx, ny = mask.shape
    center_index = tf.reduce_mean(tf.cast(tf.where(tf.cast(mask, tf.float32) > 0.1), tf.float32), axis=0)
    xx, yy = tf.meshgrid(range(ny), range(nx))
    cartesian_coordinates = tf.cast(tf.stack((xx, yy), axis=2), tf.float32) - center_index[tf.newaxis, tf.newaxis, ::-1]
    cartesian_coordinates *= fov[tf.newaxis, tf.newaxis, :] / tf.constant([nx, ny], dtype=tf.float32)[tf.newaxis, tf.newaxis, :]

    r = tf.linalg.norm(cartesian_coordinates, axis=2)
    phi = tf.math.angle(tf.complex(cartesian_coordinates[..., 0], cartesian_coordinates[..., 1]))

    polar_coordinates = tf.cast(tf.stack((r, phi), axis=-1), tf.float32)
    return polar_coordinates, cartesian_coordinates


def compute_transmural_position(pixel_coordinates: tf.Tensor, mask: tf.Tensor, n_edge_pixels: Optional[int] = 15):
    """ Calculates the relative transmural position for given pixel coordinates within the myocardium (given by mask)

    :param pixel_coordinates: (X, Y, 2)
    :param mask: (X, Y)
    :param n_edge_pixels: int -
    :return: (X, Y) where values outside the mask are set to -1.
    """
    mask_indices = tf.where(mask > 0.1)  # noqa
    center_of_mass = tf.reduce_mean(tf.gather_nd(pixel_coordinates, mask_indices), axis=0)
    centered_pixel_coords = pixel_coordinates - center_of_mass
    included_coordinates = tf.gather_nd(centered_pixel_coords, mask_indices)

    closest_inner_edge_coords, closest_outer_edge_coords = compute_distance_to_myocard_borders(centered_pixel_coords,
                                                                                               mask=mask,
                                                                                               n_edge_pixels=n_edge_pixels)

    transmural_thickness = tf.linalg.norm(closest_outer_edge_coords - closest_inner_edge_coords, axis=1, keepdims=True)
    radial_bv = (closest_outer_edge_coords - closest_inner_edge_coords) / transmural_thickness

    transmural_pos = tf.einsum('ni, ni -> n', (included_coordinates - closest_inner_edge_coords), radial_bv)
    relative_transmural_pos = transmural_pos / transmural_thickness[:, 0]
    normalized_transmural_position_2d = fill_dense_map(mask=mask, values=relative_transmural_pos, default_fill_value=-1.)
    return normalized_transmural_position_2d


def compute_local_basis_2d(pixel_coordinates: tf.Tensor, mask: tf.Tensor, n_edge_pixels: Optional[int] = 15,
                           homogeneous_handedness: bool = False,
                           long_axis_direction: Tuple[float, float, float] = (0., 0., 1.)):
    """ Computes the local coordinate basis for every pixel that is included in the myocardial mask.

    **Note:** This function assumes the coordinates of the corresponding pixels to be in perfect short-axis slice
    orientation, which means that the local longitudinal basis vector is [0, 0, 1]

    1. Calculate center of mass of all pixels in mask and center coordinates accordingly
    2. Extract coordinates of pixels belonging to mask-borders
    3. For all pixels in mask: Calculate distance to all coordinates in both mask-edges
            - if a pixel belongs to the border (exists a distance==0) pick the coordinate
            - else: average n_nearest border pixels weighted by the corresponding distance
    4. The radial basis vector is then defined as difference vector between the nearest edge-points of the inner
        and outer mask edge pointing outwards.
    5. The longitudinal basis vector is defined as [0, 0, 1]
    6. The circumferential basis vector is defined as cross product: radial_bv x longitudinal_bv

    :param pixel_coordinates: (X, Y, 2)
    :param mask: (X, Y)
    :param n_edge_pixels: (**Uneven** integer)
    :param homogeneous_handedness:  Used to compensate for different coordinate system definitions:
                        If the local basis is defined such that the circumferential basis vector has the same handedness
                         over the myocardium, the projection of the first eigenvector onto the circumferential basis
                         vector introduces a sign-flip depending on the position relative to the myocardial center of
                          mass. --> sign(x - x_com) * sign(y - y_com)
    :param long_axis_direction:

    :return: tf.Tensor (X, Y, 3, 3) Where the innermost axis indexes the basis vectors in order
                                    (radial, circumferential, longitudinal) -> (..., 0), (..., 1), (..., 2)
    """
    mask_indices = tf.where(mask > 0.1)
    center_of_mass = tf.reduce_mean(tf.gather_nd(pixel_coordinates, mask_indices), axis=0)
    centered_pixel_coords = pixel_coordinates - center_of_mass
    closest_edge_coordinates = compute_distance_to_myocard_borders(centered_pixel_coords, mask=mask,
                                                                   n_edge_pixels=n_edge_pixels)
    # Define radial basis vectors
    radial_bv = closest_edge_coordinates[1, ...] - closest_edge_coordinates[0, ...]
    radial_bv = tf.concat((radial_bv, tf.zeros_like(radial_bv[..., 0:1])), axis=-1)
    radial_bv = radial_bv / tf.linalg.norm(radial_bv, axis=-1, keepdims=True)

    # Define longitudinal basis vectors
    longitudinal_bv = tf.tile(tf.constant(long_axis_direction, dtype=tf.float32,
                                          shape=(1, 3)), [mask_indices.shape[0], 1])

    # Define circumferential basis vectors
    circumferential_bv = tf.linalg.cross(radial_bv, longitudinal_bv)

    if not homogeneous_handedness:
        circ_sign_flip = -1. * tf.sign(circumferential_bv[:, 0]) * tf.sign(circumferential_bv[:, 1])
        circ_sign_flip = tf.where(circ_sign_flip == 0, -tf.ones_like(circ_sign_flip), circ_sign_flip)
        circumferential_bv *= circ_sign_flip[:, tf.newaxis]

    # Return as 2D dense map
    local_basis = tf.stack((radial_bv, circumferential_bv, longitudinal_bv), axis=2)
    dense_map = fill_dense_map(mask, local_basis)
    return tf.transpose(dense_map, [0, 1, 3, 2])


def compute_distance_to_myocard_borders(centered_coordinates: tf.Tensor, mask: tf.Tensor, n_edge_pixels: int = 8):
    """ Computes the distances to the inner-edge and outer-edge of a ring-like myocard-mask

    use as:
    inner_edge, outer_edge = compute_distance_to_myocard_borders(...) or
    edges = compute_distance_to_myocard_borders(...)

    :param centered_coordinates: (X, Y, 2)
    :param mask: (X, Y) 2D binary mask with N entries == 1 and 0 otherwise
    :param n_edge_pixels: int = 8
    :return: (2, N, 2) where the first axis indexes the closest inner/outer edge coordinate for N points.
    """
    mask_indices = tf.where(mask > 0.1)

    included_coordinates = tf.gather_nd(centered_coordinates, mask_indices)

    # Calculate coordinates corresponding to shortest distances to inner edge:
    edge_indices_inner = tf.where(_get_contours(np.array(mask)) == 2)  # See _get_contours for explanation of const 2
    edge_coordinates = tf.gather_nd(centered_coordinates, edge_indices_inner)
    closest_inner_edge_coordinates = _nearest_coordinate_on_edge(edge_coordinates, included_coordinates,
                                                                 n_edge_pixels)

    # Calculate coordinates corresponding to shorted distances to outer edge
    edge_indices_outer = tf.where(_get_contours(np.array(mask)) == 1)  # See _get_contours for explanation of const 1
    edge_coordinates = tf.gather_nd(centered_coordinates, edge_indices_outer)
    closest_outer_edge_coordinates = _nearest_coordinate_on_edge(edge_coordinates, included_coordinates, n_edge_pixels)

    return tf.stack((closest_inner_edge_coordinates, closest_outer_edge_coordinates), axis=0)


def _get_contours(mask: np.ndarray):
    """Finds contours of a binary image of a ring-like mask. Returns an equally shaped array with pixel values,
     belonging to the outer / inner circle set to 1 / 2
    :param mask: nd.array of shape (X, Y)
    :return:
    """
    mask_int = np.uint8(mask)
    cnts, hierarchy = cv2.findContours(mask_int, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[-2:]
    ret_img = np.zeros(mask_int.shape, dtype=np.uint8)
    cv2.drawContours(ret_img, cnts, 0, (1, 1, 1), 1)
    cv2.drawContours(ret_img, cnts, 1, (2, 2, 2), 1)
    return ret_img


def _nearest_coordinate_on_edge(edge_coordinates: tf.Tensor, included_coordinates: tf.Tensor, n_edge_pixels: int = 20):
    """ Calculates the distance between all coordinates in argument included_coordinates and all edge_pixels. If
    the smallest distance is equal to zero, the function returns the exact coordinate. Otherwise the weighted (by
    distance) average of the n_edge_pixels nearest edge coordinates is returned.

    :param edge_coordinates: (N, 3)
    :param included_coordinates: (M, 3)
    :param n_edge_pixels: int
    :return: (M, 3)
    """
    distances = tf.linalg.norm(included_coordinates[:, tf.newaxis, :] - edge_coordinates[tf.newaxis, :, :], axis=-1)
    sorted_distances_indices = tf.argsort(distances, axis=1, direction='ASCENDING')[:, 0:n_edge_pixels]
    neighbour_coordinates = tf.gather(edge_coordinates, sorted_distances_indices, axis=0)
    shortest_distances = tf.gather(distances, sorted_distances_indices, batch_dims=1)

    # Average over n_edge_pixels coordinates:
    _ = tf.math.divide_no_nan(neighbour_coordinates, shortest_distances[:, :, tf.newaxis])
    normalization_factor = 1. / tf.reduce_sum(tf.math.divide_no_nan(1., shortest_distances), axis=1, keepdims=True)
    avg_coords = tf.reduce_sum(_, axis=1) * normalization_factor
    return avg_coords
