__all__ = ['fill_dense_map', 'flatten_tensor', 'flat_tensor_to_matrix', 'is_symmetric',
           'transmural_histogram']

from typing import Optional
import tensorflow as tf
import numpy as np


def fill_dense_map(mask: tf.Tensor, values: tf.Tensor, default_fill_value: Optional = 0.):
    """ Takes in a Tensor that represents N, (...)-dimensional entries in a 2D map of the same shape as argument mask
     and returns a dense 2Dx(...) - map.
    :param mask: (X, Y) binary mask
    :param values: (N, ...) flat Tensor of ... dimensional features where N < tf.reduce_sum(mask)
    :param default_fill_value:
    :return: (X, Y, ...)
    """

    value_dims = tf.shape(values)[1:]
    mask_shape = tf.shape(mask)
    expand_shape = tf.concat((mask_shape, tf.ones_like(value_dims)), axis=0)
    expanded_mask = tf.reshape(mask, expand_shape)
    tiling_shape = tf.concat((tf.ones_like(mask_shape), value_dims), axis=0)
    sparse_indices = tf.where(tf.tile(expanded_mask, tiling_shape))

    dense_shape = tf.cast(tf.concat((mask_shape, value_dims), axis=0), tf.int64)
    flat_values = tf.reshape(values, [-1])
    sparse_result = tf.sparse.SparseTensor(indices=sparse_indices, values=flat_values, dense_shape=dense_shape)
    dense_result = tf.sparse.to_dense(sparse_result, default_value=default_fill_value)
    return dense_result


def flatten_tensor(tensors: tf.Tensor):
    """ Reshapes a batch of symmetric 3x3 tensors to a flat representation
    containing only the unique entries.

    :param tensors: (... 3, 3)
    :return: (..., 6) [D_xx, D_yy, D_zz, Dxy, Dxz, Dyz]
    """
    batch_shape, tensors, squeeze_output = _get_batch_shape(tensors)
    expansion_dims = tf.ones((tf.size(batch_shape)), dtype=tf.int32)
    flat_idx = tf.constant([[0, 0], [1, 1], [2, 2], [0, 1], [0, 2], [1, 2]],
                           shape=tf.concat((expansion_dims, [6, 2]), axis=0),
                           dtype=tf.int32)

    batch_flat_idx = tf.tile(flat_idx, tf.concat((batch_shape, [1, 1]), axis=0))
    flat_symmetric_tensors = tf.gather_nd(tensors, batch_flat_idx, batch_dims=tf.size(batch_shape))

    if squeeze_output:
        flat_symmetric_tensors = tf.squeeze(flat_symmetric_tensors)

    return flat_symmetric_tensors


def flat_tensor_to_matrix(flat_tensors: tf.Tensor):
    """ Reshapes a batch of vectors of length 6 (containing the 6 unique values
    of a symmetric 3x3 matrix) into square matrices

    :param flat_tensors: (..., 6) [D_xx, D_yy, D_zz, Dxy, Dxz, Dyz]
    :return: (..., 3, 3)
    """
    row_1 = tf.stack([flat_tensors[..., 0], flat_tensors[..., 3], flat_tensors[..., 4]], axis=-1)
    row_2 = tf.stack([flat_tensors[..., 3], flat_tensors[..., 1], flat_tensors[..., 5]], axis=-1)
    row_3 = tf.stack([flat_tensors[..., 4], flat_tensors[..., 5], flat_tensors[..., 2]], axis=-1)
    symmetric_matrix = tf.stack([row_1, row_2, row_3], axis=-1)
    return symmetric_matrix


def is_symmetric(tensors: tf.Tensor):
    """

    :param tensors: (..., 3, 3)
    :return: bool (...)
    """
    batch_shape, tensors, squeeze_output = _get_batch_shape(tensors)
    n_batch_dims = tf.size(batch_shape)
    transposed_tensors = tf.transpose(tensors, tf.concat([tf.range(0, n_batch_dims), [n_batch_dims+1, n_batch_dims]], axis=0))
    conditional = tf.reduce_sum(tf.abs(transposed_tensors - tensors), axis=(n_batch_dims, n_batch_dims+1))
    integer_mask = tf.where(conditional < 1e-10, tf.ones_like(conditional), tf.zeros_like(conditional))

    if squeeze_output:
        integer_mask = tf.squeeze(integer_mask)

    return tf.cast(integer_mask, tf.bool)


def _get_batch_shape(tensors: tf.Tensor):
    if tf.size(tf.shape(tensors)) == 2:
        batch_shape = tf.constant([1, ], dtype=tf.int32)
        tensors = tensors[tf.newaxis, :, :]
        squeeze = True
    else:
        batch_shape = tf.shape(tensors)[0:-2]
        squeeze = False

    return batch_shape, tensors, squeeze


def transmural_histogram(tm_depth: np.ndarray, scalar_measure: np.ndarray, n_bins: int = 5):
    """ Calculates the histogram of a scalar map for a given transmural depth map.

    :param tm_depth: 2D map containing values between 0 and 1 for valid pixels inside the LV.
    :param scalar_measure: 2D scalar map corresponding to the transmural depth map
    :param n_bins: (default: 5) number of bins (for transmural depth) used to calculate the
                    histogram on the scalar map.
    :return: (Values per bin, Mean per bin, Standard deviations per bin, bin_centers)
             np.ndarray, np.ndarray, np.ndarray, np.ndarray
    """

    bins = np.linspace(0., 1., n_bins + 1)
    bin_diffs = np.diff(bins)
    bin_centers = bins[:-1] + bin_diffs / 2

    values, means, stds = [], [], []
    for b, delta in zip(bins, bin_diffs):
        condition = np.logical_and(tm_depth >= b, tm_depth < b + delta)
        vals = scalar_measure[np.where(condition)]
        values.append(vals), means.append(np.nanmean(vals)), stds.append(np.nanstd(vals))

    return values, np.array(means), np.array(stds), bin_centers


def get_sector_masks(mask: np.ndarray, n_sectors: int = 8, angular_offset: float = 0,
                     transmural_bins: int = 1, fill_value: float = np.nan,
                     anteroseptal_insertion_point: np.ndarray = None):
    """ Evenly splits provided mask into sectors by polar angle and rings by transmurality,
    and returns a 2D mask containing the categorical labels.

    Label value is increasing first in angular direction, then in transmural direction

    :param mask: (X, Y)
    :param n_sectors: int - number of sectors subdividing 360 degrees
    :param angular_offset: in degree
    :param transmural_bins: int - number of rings the mask is subdivided into, in transmural direction
    :param fill_value: value for background pixels in the returned mask
    :param anteroseptal_insertion_point: 2D coordinate of anterior insertionpoint, used 
                                         to calculate the angular offset if specified
    :return: (X, Y) integer masks
    """
    from cdtipy.coordinates import get_relative_polar_coordinates
    
    if anteroseptal_insertion_point is not None:
        tmp_coords = np.stack(np.meshgrid(*[range(s) for s in mask.shape[::-1]], indexing='xy'), axis=-1)
        center_of_mass = np.mean(tmp_coords[np.where(mask > 0)], axis=0)
        dx, dy = (anteroseptal_insertion_point - center_of_mass)
        angular_offset = np.rad2deg(np.angle(dy + 1j * dx) + np.pi/2)
        angular_offset += 90
        
    mask_out = np.zeros_like(mask) * fill_value
    polar_coordinates = get_relative_polar_coordinates(mask.astype(np.int8),
                                                       fov=np.array(mask.shape),
                                                       offset_angle=angular_offset,
                                                       inner_border_offset=0.)
    transmurality = polar_coordinates[..., 0]
    polar_angle = np.rad2deg(polar_coordinates[..., 1] + np.pi)
    
    transmurality_bins = np.linspace(0, 1, transmural_bins + 1)
    angle_bins = np.linspace(0, 360, n_sectors + 1)

    for td_idx, td_left, td_right in zip(range(transmural_bins), transmurality_bins[:-1], transmurality_bins[1:]):
        td_conditional = np.logical_and(transmurality >= td_left, transmurality < td_right)
        sector_offset = td_idx * n_sectors
        for sector_idx, ang_left, ang_right in zip(range(n_sectors), angle_bins[:-1], angle_bins[1:]):
            angle_conditional = np.logical_and(polar_angle >= ang_left, polar_angle < ang_right)
            conditional = np.prod(np.stack([td_conditional, angle_conditional, mask], axis=0), axis=0) > 0.5
            mask_out[np.where(conditional)] = sector_idx + 1 + sector_offset    
    return mask_out
