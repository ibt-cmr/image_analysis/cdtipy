__all__ = ["file_selection", "PlotInAccordion", "display_header", "export_parrec",
           "data_checker"]

import os
from typing import Union, Iterable, Tuple
from glob import glob
import copy
import json
from warnings import warn
import itertools
import math
from IPython.display import display, JSON

import ipywidgets
import numpy as np
from parrec.parread import Parread

import cdtipy.io
import cmr_widgets


def file_selection(parameter_dict: dict, data_folder: str = "./",
                   include_pattern: Union[str, Iterable[str]] = ("dti", "diff"),
                   exclude_pattern: Union[str, Iterable[str]] = None):
    """ Recursively searches for par/rec files inside the specified folder whose filename contains
    either 'diff' or 'dti'. Creates an ipython dropdown widget listing all found files.

    :param parameter_dict:
    :param data_folder:
    :param include_pattern:
    :param exclude_pattern:
    :return:
    """
    if not os.path.exists(os.path.abspath(data_folder)):
        raise ValueError("Specified Data folder does not exist. Please check your input")

    if isinstance(include_pattern, str):
        include_pattern = (include_pattern, )

    if exclude_pattern is not None and isinstance(exclude_pattern, str):
            exclude_pattern = (exclude_pattern, )
    
    # Create Widgets
    folder_widget = ipywidgets.Text(value=os.path.abspath(data_folder),
                                    placeholder='Type path to data folder',
                                    description='Path to data:', disabled=False)
    text_layout = ipywidgets.Layout(width='100%', height='25px')
    study_widget = ipywidgets.Text(value="", description='Study Name:', disabled=True,
                                   layout=text_layout)
    measurement_widget = ipywidgets.Text(value="", description='Measurement Name:', disabled=True,
                                         layout=text_layout)
    file_widget = ipywidgets.Text(value="", description='File:', disabled=True,
                                  layout=text_layout)
    export_widget = ipywidgets.Text(value="", description='Python Export dir:', disabled=False,
                                    layout=text_layout)
    header_output = ipywidgets.Accordion(children=[ipywidgets.Output()], selected_index=None)
    header_output.set_title(0, "Header information")

    dummy_values = ["Searching for files...", ]
    data_selector = ipywidgets.Dropdown(options=dummy_values, value=dummy_values[0],
                                        description='Select dataset:',
                                        style={'description_width': 'initial'},
                                        disabled=False,
                                        layout=ipywidgets.Layout(width='100%', height='25px'))

    export_button = ipywidgets.Button(description="Export to python")
    inspect_data_button = ipywidgets.Button(description="Preview Rec-Data")
    image_output = ipywidgets.Output()

    # Set up folder widget listener
    def _update_files(*args):
        data_folder = os.path.abspath(folder_widget.value)
        folder_widget.value = data_folder
        data_names = [f.replace('.par', '') for f in glob(f'{data_folder}/**/*.par', recursive=True) if
                      any(s in os.path.basename(f) for s in include_pattern)]
        if exclude_pattern is not None:
            data_names = [n for n in data_names if not any(s in n for s in exclude_pattern)]
        data_selector.options = tuple(data_names)

    folder_widget.observe(_update_files)
    _update_files()

    # Set up dropdown listener
    def _update_names(*args):
        study_path = os.path.dirname(os.path.dirname(data_selector.value))
        study_widget.value = study_path
        measurement_name = os.path.basename(os.path.dirname(data_selector.value))
        measurement_widget.value = measurement_name
        file_widget.value = os.path.basename(data_selector.value)
        export_p = f"{data_folder}/python_export/{os.path.basename(study_path)}_{measurement_name}/"
        export_widget.value = os.path.abspath(export_p)

        image_output.clear_output()
        inspect_data_button.disable = False

        header_output.children[0].clear_output()

        reader = Parread(f"{data_selector.value}.par")
        temp = reader.read()
        temp["ImageInformation"] = {k: [img[k] for img in temp["ImageInformation"]]
                                    for k in temp["ImageInformation"][0].keys()}
        with header_output.children[0]:
            par_dict_json = copy.deepcopy(temp)
            outputs = [ipywidgets.Output(layout=ipywidgets.Layout(width="600px")) for _ in range(2)]
            hbox = ipywidgets.HBox(outputs)
            display(hbox)
            with outputs[1]:
                s1 = par_dict_json.pop("ImageInformation")
                display(JSON(s1, root="ImageInformation"))
            with outputs[0]:
                display(JSON(par_dict_json, root="MetaInfo"))

        parameter_dict.update(temp)

    data_selector.observe(_update_names)
    _update_names()

    # Set up button listeners
    def _on_export(*args):
        export_parrec(data_selector.value, python_export_dir=export_widget.value)
    export_button.on_click(_on_export)

    def _on_image_show(*args):
        # Export scanner reconstructed data
        from parrec.recread import Recread
        reader = Recread(f"{data_selector.value}.rec")
        rec_data = np.abs(np.squeeze(reader.read()))
        with image_output:
            slide_show = cmr_widgets.SlideShow(rec_data[:, :, np.newaxis], image_axes=(0, 1))
            slide_show.figure.tight_layout()
            slide_show.figure.axes[0].axis("off")
        inspect_data_button.disable = True
        display(image_output)
    inspect_data_button.on_click(_on_image_show)

    # Arange and display all
    button_box = ipywidgets.HBox([export_button, inspect_data_button])
    vbox = ipywidgets.VBox([folder_widget, data_selector, study_widget,
                            measurement_widget, file_widget, export_widget,
                            button_box])

    total_box = ipywidgets.GridspecLayout(1, 3)
    total_box[0, :2] = vbox
    total_box[0, 2] = image_output

    combibox = ipywidgets.VBox([total_box, header_output])

    display(combibox)
    return folder_widget, data_selector, export_widget, study_widget, measurement_widget


class PlotInAccordion(object):
    def __init__(self, title: str):
        self._title = title

    def __enter__(self):
        acc = ipywidgets.Accordion(children=[ipywidgets.Output()], selected_index=None)
        display(acc)
        acc.set_title(0, self._title)
        return acc.children[0].__enter__()

    def __exit__(self, *args):
        return


def display_header(par_file: str) -> dict:
    """

    :param par_file: path to file without ending. Corresponds to selector widget value
    :return: dictionary containing header information
    """
    reader = Parread(f"{par_file}.par")
    par_dict = reader.read()
    par_dict["ImageInformation"] = {k: [img[k] for img in par_dict["ImageInformation"]] for k in
                                    par_dict["ImageInformation"][0].keys()}

    with PlotInAccordion("Par file Json redering"):
        par_dict_json = copy.deepcopy(par_dict)
        outputs = [ipywidgets.Output(layout=ipywidgets.Layout(width="600px")) for _ in range(2)]
        hbox = ipywidgets.HBox(outputs)
        display(hbox)
        with outputs[1]:
            s1 = par_dict_json.pop("ImageInformation")
            display(JSON(s1, root="ImageInformation"))
        with outputs[0]:
            display(JSON(par_dict_json, root="MetaInfo"))

    return par_dict


def export_parrec(parrec_path: str, python_export_dir: str):
    """

    :param parrec_path:
    :param python_export_dir:
    :return:
    """
    os.makedirs(python_export_dir, exist_ok=True)

    # Export parameter header as json
    from parrec.parread import Parread
    reader = Parread(f"{parrec_path}.par")
    par_dict = reader.read()
    par_dict["ImageInformation"] = {k: [img[k] for img in par_dict["ImageInformation"]] for k in
                                    par_dict["ImageInformation"][0].keys()}

    with open(f"{python_export_dir}/par_dict.json", "w+") as jsfile:
        json.dump(par_dict, jsfile)

    # Export scanner reconstructed data
    from parrec.recread import Recread
    reader = Recread(f"{parrec_path}.rec")
    rec_data = np.squeeze(reader.read())
    np.save(f"{python_export_dir}/rec_data.npy", rec_data)

    # Export b-vectors:
    b_vectors = cdtipy.io.bvec_mps_from_header(par_dict)
    np.savetxt(f"{python_export_dir}/bvectors_slice.txt", b_vectors,
               header="Diffusion directions scaled with sqrt(b)")


def data_checker(data, export_dir: str, max_intesity_scaling: float = 0.5):
    """ Allows to check the data for unusuable single images and create a copy
    where the corrupted images are replaced by other images from the dataset
    defined by the user.

    :param data: shape (X, Y, Slice, )
    :param export_dir:
    """
    magnitude = np.abs(data).reshape(*(data.shape[0:2]), -1) / np.max(
        np.abs(data)) / max_intesity_scaling * 2 * np.pi
    phase = np.angle(data).reshape(*data.shape[0:2], -1) + np.pi
    imgs = np.concatenate([magnitude, phase], axis=1)

    image_output = ipywidgets.Output()
    with image_output:
        slide_show = cmr_widgets.SlideShow(imgs, cmaps="gray", image_axes=(0, 1),
                                           clims=np.array([[dict(vmin=0, vmax=2*np.pi)]]))
        slide_show.axes[0, 0].axis("off")
        slide_show.figure.set_size_inches(9, 5)
        slide_show.figure.tight_layout()

    def _upd_slider(event):
        desc = event["owner"].description
        i = int(desc.split(":")[0])
        slide_show.sliders[0].value = i

    index_tuple = list(itertools.product(*[range(i) for i in data.shape[2:]]))
    options = [ipywidgets.Checkbox(description=f"{i}: {idx_tup}", value=True) for i, idx_tup in
               enumerate(index_tuple)]
    replacement = [ipywidgets.IntText(value=-1, layout={"max_width": '100px'}) for i, _ in
                   enumerate(index_tuple)]
    for i, opt in enumerate(options):
        opt.observe(_upd_slider)

    option_box = ipywidgets.GridBox([ipywidgets.HBox([o, r]) for o, r in zip(options, replacement)],
                                    layout={'overflow': 'scroll', "max_height": '500px',
                                            "max_width": '350px'})
    hbox = ipywidgets.HBox([image_output, option_box])
    save_button = ipywidgets.Button(description="Save")
    load_button = ipywidgets.Button(description="Load")
    export_text = ipywidgets.Text(value=export_dir, description='Export dir:', disabled=False,
                                  layout=ipywidgets.Layout(width='100%', height='25px'))
    button_box = ipywidgets.HBox([save_button, load_button, export_text])
    vbox = ipywidgets.VBox([hbox, button_box])

    def _save(*args):
        invalid_data_indices = [
            opt.description.split(":")[1] + ":" + options[rep.value].description.split(":")[1]
            for opt, rep in zip(options, replacement) if not opt.value]
        with open(f"{export_dir}/invalid_indices_replacements.txt", "w+") as file:
            file.write("\n".join(invalid_data_indices))

        save_dat = data.copy()
        for temp in invalid_data_indices:
            left_idx, right_idx = [i.replace("(", "").replace(")", "").split(",") for i in
                                   temp.split(":")]
            left_idx, right_idx = [tuple(int(i) for i in _) for _ in [left_idx, right_idx]]
            _ = data[(slice(0, data.shape[0]), slice(0, data.shape[1])) + right_idx]
            save_dat[(slice(0, data.shape[0]), slice(0, data.shape[1])) + left_idx] = _
        np.save(f"{export_dir}/unregistered_cleaned_data.npy", save_dat)
        del save_dat

    save_button.on_click(_save)

    def _load(*args):
        with open(f"{export_dir}/invalid_indices_replacements.txt", "r") as file:
            lines = file.read().split("\n")

        for temp in lines:
            left_idx, right_idx = [i.replace("(", "").replace(")", "").split(",") for i in
                                   temp.split(":")]
            left_idx, right_idx = [tuple(int(i) for i in _) for _ in [left_idx, right_idx]]
            li = np.ravel_multi_index(left_idx, data.shape[2:])
            ri = np.ravel_multi_index(right_idx, data.shape[2:])

            options[li].value = False
            replacement[li].value = ri

    load_button.on_click(_load)
    display(vbox)
    return vbox
