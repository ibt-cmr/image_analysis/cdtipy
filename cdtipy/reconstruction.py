__all__ = ["get_recon_indices", "call_MRecon"]

import os
import numpy as np
import scipy.io as sio
from glob import glob

try:
    import matlab.engine
except ModuleNotFoundError:
    import warnings
    warnings.warn("No matlab installation found!")


def get_recon_indices(par_dict: dict):
    """
    """

    diff_dir_index = (np.array(par_dict["ImageInformation"]["GradientOrientationNumber"]) - 1)[
                     ::par_dict["MaxNumberOfSlicesLocations"]]
    diff_dir_index = diff_dir_index[:len(diff_dir_index) // par_dict["MaxNumberOfDynamics"]]

    bval_index = (np.array(par_dict["ImageInformation"]["DiffusionBValueNumber"]) - 1)[
                 ::par_dict["MaxNumberOfSlicesLocations"]]
    bval_index = bval_index[:len(bval_index) // par_dict["MaxNumberOfDynamics"]]

    assert len(bval_index) == len(diff_dir_index)
    return bval_index.reshape(1, -1), diff_dir_index.reshape(1, -1)


def call_MRecon(mrecon_path: str, study_path: str,
                measurement_name: str, par_dict: dict,
                ref_scan_path: str = None, recon_type: int = 1):
    """ Calls MRecon script that reconstructs all averages separately and saves them into mat file.
    Loads the .mat files and returns the image data as numpy arrays.

    *Requires*: Licence of MRecon and binding to matlab instance

    :param mrecon_path: path to mrecon installation with valid license
    :param study_path:
    :param measurement_name:
    :param par_dict:
    :param ref_scan_path: if not provided, assumes that there is a directory
                            '1000-SenseRefScan' in the directory os.path.dirname(study_path)
    :param recon_type: from [1, 2] (within/outside) gating window
    :return:
    """
    # start matlab engine and add required code to the MATLAB-PATH
    eng = matlab.engine.start_matlab()
    matlab_code_path = os.path.abspath(os.path.dirname(__file__)) + "/matlab_code"
    eng.addpath(eng.genpath(os.path.abspath(matlab_code_path)))
    eng.addpath(eng.genpath(os.path.abspath(mrecon_path)))

    # Infer file paths
    if ref_scan_path is None:
        ref_scan_path = os.path.abspath(glob(f"{study_path}/1000-SenseRefScan/*.raw")[0])
    raw_file_path = os.path.abspath(glob(f"{study_path}/{measurement_name}/*.raw")[0])

    assert os.path.exists(ref_scan_path) and os.path.exists(raw_file_path)

    bval_index, diff_dir_index = get_recon_indices(par_dict)
    print(diff_dir_index, bval_index, ref_scan_path, raw_file_path,  f"{study_path}/{measurement_name}")

    result_file = eng.call_recon(ref_scan_path, raw_file_path,
                                 f"{study_path}/{measurement_name}",
                                 matlab.double(diff_dir_index.tolist()[:2]),
                                 matlab.double(bval_index.tolist()[:2]),
                                 matlab.double(recon_type), nargout=1)

    data_real, data_imag = [sio.loadmat(result_file, variable_names=[key])[key]
                            for key in ["data_real", "data_imag"]]
    data = data_real + 1j * data_imag

    # Reshape into (X, Y, Z, bvec, dynamics/averages)
    # It comes as (X, Y, dyns, Z, averages, bvecs) where dyns is squeezed in case of a single dynamic
    if par_dict["MaxNumberOfDynamics"] > 1 and recon_type == 1:
        data = np.swapaxes(data, 2, 3)
        data = data.reshape(*data.shape[0:3], -1, *data.shape[5:])
    data = np.swapaxes(data, 3, 4)

    return data