"""Collection of funtions to quiver plot local coordinate systems inside masks. Meant to
check intermediate calculations"""
__all__ = ['quiver_plot_inplane', 'quiver_plot_3d']

from typing import Tuple
import itertools

# from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
import numpy as np
import cdtipy.coordinates


def quiver_plot_inplane(local_basis: np.ndarray, mask: np.ndarray, fov: np.ndarray, axis=None):
    """ Quiver plot the first and second vectors of the local coordinate basis under the
    assumption of (radia, circular, longitudinal ordering)

    :param local_basis: (X, Y, 3, 3) where the innermost dimension indexes the different basis vectors
    :param mask: (X, Y)
    :param axis: plt.Axis
    :return:
    """
    _, coords = cdtipy.coordinates.get_mask_centered_coordinates(mask, fov)
    xind, yind = coords[..., 0].numpy(), coords[..., 1].numpy()
    xind, yind, local_basis = [_[np.where(mask > 0.01)] for _ in [xind, yind, local_basis]]

    radial_basis_vectors, circular_basis_vectors = local_basis[..., 0], local_basis[..., 1]

    if axis is None:
        f, axis = plt.subplots(1, 1)

    axis.quiver(xind, yind, radial_basis_vectors[..., 0], -radial_basis_vectors[..., 1], color='r')
    axis.quiver(xind, yind, circular_basis_vectors[..., 0], -circular_basis_vectors[..., 1], color='g')
    axis.legend(['radial', 'circular'])
    axis.invert_yaxis()
    return axis


def quiver_plot_3d(local_basis: np.ndarray, mask: np.ndarray, fov: np.ndarray, step=10, scale=100):
    """ 3D - Quiver plots local coordinate basis in mask

    :param local_basis:(X, Y, 3, 3)
    :param mask: (X, Y)
    :param step: only plots every nth entry of the argument
    :return:
    """
    _, coords = cdtipy.coordinates.get_mask_centered_coordinates(mask, fov)
    xind, yind = coords[..., 0].numpy(), coords[..., 1].numpy()
    xind, yind, local_basis = [_[np.where(mask > 0.01)] for _ in [xind, yind, local_basis]]
    zind = np.zeros_like(xind)

    fig = plt.figure()
    axis = fig.gca(projection='3d')
    rx, ry, rz = [local_basis[..., i, 0] / scale for i in range(3)]
    cx, cy, cz = [local_basis[..., i, 1] / scale for i in range(3)]
    lx, ly, lz = [local_basis[..., i, 2] / scale for i in range(3)]
    axis.set_zlim([-4, 4])
    axis.quiver(xind[::step], yind[::step], zind[::step], rx[::step], ry[::step], rz[::step], color='r')
    axis.quiver(xind[::step], yind[::step], zind[::step], cx[::step], cy[::step], cz[::step], color='g')
    axis.quiver(xind[::step], yind[::step], zind[::step], lx[::step], ly[::step], lz[::step], color='b')
    axis.legend(['radial', 'circular', 'longitudinal'])
    return axis


def get_quiver2d_update(axes: np.ndarray, vector_data: np.ndarray, axes_indices: Tuple[int, int],
                        masks: np.ndarray, color_range: np.ndarray = None, scale: float = None, 
                        cmap: str = "viridis"):
    """
    :param axes: 2D array of plt.Axes (rows, columns) as obtained from subplots
    :param vector_data: (X, Y, ..., 2) 2D vector data to be quiver-plotted
    :param axes_indices: indices of the data dimensions corresponding to the rows/columns of the axes array.
                            for singular rows/cols, the value mus be None. E.g. for 3 columns and one row,
                            where the second (0-based) data dimension is corresponding to the columns,
                            specify: (None, 2)
    :param masks: (X, Y, *axes.shape)
    """
    nx, ny = vector_data.shape[:2]
    all_coords = np.stack(np.meshgrid(range(ny), range(nx), indexing="xy"), axis=-1)

    assert len(axes.shape) == 2
    nrows, ncols = axes.shape

    # initialize quiver plots
    quiver_artists = []
    masked_indices = []
    n_sliders = len(vector_data.shape) - 3 - sum([i is not None for i in axes_indices])
    initial_slice = (slice(0, nx), slice(0, ny), *[0 for _ in range(n_sliders)], slice(0, 2))
    for linear_idx, (rr, cc) in enumerate(itertools.product(range(nrows), range(ncols))):
        indices_2d = np.where(masks[..., rr, cc])
        positions = all_coords[indices_2d]

        vectors_slice = vector_data
        for ii, jj in zip((rr, cc), axes_indices):
            if jj is not None:
                vectors_slice = np.take(vectors_slice, ii, jj)

        vectors_slice = vectors_slice[initial_slice][indices_2d]
        vector_norm = np.linalg.norm(vectors_slice, axis=-1, keepdims=True)
        vectors_slice /= vector_norm

        cnorm = None
        if color_range is not None:
            cnorm = Normalize(vmin=color_range[rr, cc, 0], vmax=color_range[rr, cc, 1])

        art = axes[rr, cc].quiver(*positions.T, *vectors_slice.T, np.squeeze(vector_norm),
                                  norm=cnorm, scale=scale, cmap=cmap)
        quiver_artists.append(art), masked_indices.append(indices_2d)

    # create update function
    def update_quiver(*slider_values):
        data_slice = (slice(0, nx), slice(0, ny), *slider_values, slice(0, 2))

        for linear_idx, (rr, cc) in enumerate(itertools.product(range(nrows), range(ncols))):
            vectors_slice = vector_data
            for ii, jj in zip((rr, cc), axes_indices):
                if jj is not None:
                    vectors_slice = np.take(vectors_slice, ii, jj)

            vectors_slice = vectors_slice[data_slice][masked_indices[linear_idx]]
            vector_norm = np.linalg.norm(vectors_slice, axis=-1, keepdims=True)
            vectors_slice /= vector_norm
            quiver_artists[linear_idx].set_UVC(*vectors_slice.T, np.squeeze(vector_norm))

    return update_quiver, quiver_artists


def get_quiver2d_update_changing_mask(axes: np.ndarray, vector_data: np.ndarray,
                                      masks: np.ndarray, color_range: np.ndarray = None,
                                      scale: float = None):
    """
    :param axes: 2D array of plt.Axes (rows, columns) as obtained from subplots
    :param vector_data: (X, Y, T, 2) 2D vector data to be quiver-plotted
    :param axes_indices: indices of the data dimensions corresponding to the rows/columns of the axes array.
                            for singular rows/cols, the value mus be None. E.g. for 3 columns and one row,
                            where the second (0-based) data dimension is corresponding to the columns,
                            specify: (None, 2)
    :param masks: (X, Y, T)
    """
    nx, ny, ntimes = vector_data.shape[:3]
    all_coords = np.stack(np.meshgrid(range(ny), range(nx), indexing="xy"), axis=-1)

    cnorm = None
    if color_range is not None:
        cnorm = Normalize(vmin=color_range[0], vmax=color_range[1])

    # initialize quiver plots
    vectors = []
    vector_norms = []
    points = []
    for time_index in range(ntimes):
        indices_2d = np.where(masks[..., time_index])
        positions = all_coords[indices_2d]

        vectors_slice = vector_data[:, :, time_index][indices_2d]
        vector_norm = np.linalg.norm(vectors_slice, axis=-1, keepdims=True)
        vectors_slice /= vector_norm
        points.append(positions), vectors.append(vectors_slice), vector_norms.append(vector_norm)

    art = axes.quiver(*points[0].T, *vectors[0].T, np.squeeze(vector_norms[0]), norm=cnorm, scale=scale)
    axes._quiver_artist = art

    # create update function
    def update_quiver(slider_value):
        axes._quiver_artist.remove()
        art = axes.quiver(*points[slider_value].T, *vectors[slider_value].T,
                          np.squeeze(vector_norms[slider_value]), norm=cnorm, scale=scale)
        axes._quiver_artist = art

    return update_quiver