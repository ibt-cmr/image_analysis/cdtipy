__all__ = ["ResultSummary"]
from typing import Tuple
from IPython.display import display, clear_output

import matplotlib.pyplot as plt
import numpy as np
import ipywidgets

import cmr_widgets
import cdtipy



class ResultSummary:
    """Creates a Tab widget, where each tab contains one of the following figures/interactive
    widgets:

    1. Data slide show, with ROI based analysis along channels
    2. Color coded Elliptic glyphs for tensors as overlay on top of the data
    3. Histograms and overlay maps for MD, FA, HA and E2A
    4. Histograms for MD, FA, HA and E2A and the three eigenvalues
    5. Illustration of the local basis used the angle computation
    6. Interactive widget to draw ROIS and lines along which the metric of choice is evaluated

    To control which tabs are populated use the `tabs_to_plot` argument.

    :param data: magnitude data of shape (X, Y, #slices, b-values, averages)
    :param masks: Binary masks of shape (X, Y, #slices)
    :param metrics_per_slice: List of dictionary (one dict per slice) containing the combined results
                        of the cdtipy metrics_set and angulation_set functions
    :param crop_percent: Tuple of floats between 0 and 1 cropping image data according to following
                            order (bottom, top, left, right)
    :param tabs_to_plot: Tuple of floats controlling which tabs to populate
    :param metric_limits: dict(keys, (float, float))
    :param slice_labels: tuple of names corresponding to the #slices dimension
    """

    def __init__(self, data, masks, metrics_per_slice,
                 crop_percent: Tuple[float, float, float, float] = (0., 1., 0., 1),
                 tabs_to_plot: Tuple[bool, ...] = (True, False, True, False, False, False),
                 metric_limits: dict = None,
                 slice_labels: Tuple[str, ...] = ("Apical", "Mid", "Basal")
                 ):

        self._metric_limits = dict(MD=(0.7e-3, 2.7e-3), FA=(0.15, 0.55), HA=(-90, 90), E2A=(0, 90),
                                   EV1=(1.2e-3, 2.8e-3), EV2=(0.8e-3, 1.9e-3), EV3=(0.4e-3, 1.5e-3))
        if metric_limits is not None:
            self._metric_limits.update(**metric_limits)

        self._slice_labels = slice_labels

        ix, jx, iy, jy = [int(i * k) for i, k in zip(crop_percent,
                                                     [masks.shape[0], masks.shape[0],
                                                      masks.shape[1],
                                                      masks.shape[1]])]
        self._crop_percent = crop_percent
        self.cropped_slice = (slice(ix, jx), slice(iy, jy))
        self.tab, context_manager = self._set_layout(tabs_to_instantiate=tabs_to_plot)

        assert len({data.shape[2], masks.shape[2], len(metrics_per_slice)}) <= 1

        self._data = data
        self._masks = masks
        self._all_metrics = metrics_per_slice
        self.figures = {}
        self._axes = {}

        tab_funcs = [self._data_slide_show_tab, self._glyph_projections,
                     self._metric_imshow_tab, self._extended_hist_tab,
                     self._local_basis_tab, self._metric_analyzer_tab]
        for b, func in zip(tabs_to_plot, tab_funcs):
            if b:
                with next(context_manager):
                    func()

    @staticmethod
    def _set_layout(tabs_to_instantiate: Tuple[bool, ...]):
        tab_titles = (
        'Input Data', 'Eigenvector projections', 'Metric / Angulation maps', 'Histograms',
        'Local Basis', "Metric Analyzer")
        filtered_tab_titles = [t for t, f in zip(tab_titles, tabs_to_instantiate) if f]
        tab = ipywidgets.Tab(children=[ipywidgets.Output() for _
                                       in range(sum(tabs_to_instantiate))])
        context_generator = (c for c in tab.children)
        [tab.set_title(i, t) for i, t in enumerate(filtered_tab_titles)]
        display(tab)
        return tab, context_generator

    def _data_slide_show_tab(self):
        clear_output()
        sl_out = ipywidgets.VBox([ipywidgets.Output(), ipywidgets.Output()],
                                 layout={"width":"40%", "overflow":"scroll"})
        slide_show = cmr_widgets.SlideShow(self._data[self.cropped_slice],
                                           cmaps="gray", image_axes=(0, 1),
                                           slider_context=sl_out.children[1],
                                           figure_context=sl_out.children[0],
                                           slider_kws=dict(orientation="horizontal"),
                                           slider_container_type=ipywidgets.VBox,
                                           slider_names=["Slice", "Diffusion", "Average"],

                                           )
        slide_show.figure.tight_layout()
        tab = ipywidgets.Tab([ipywidgets.Output(),
                              ipywidgets.HBox([ipywidgets.Output(layout={"width": "35%", "overflow": "scroll"}),
                                                ipywidgets.Output(layout={"width": "65%", "overflow": "scroll"})])],
                              layout={"width":"60%", "overflow":"scroll"})

        for idx, title in enumerate(["Image Adjust", "Analyzer"]):
            tab.set_title(idx, title)

        hbox = ipywidgets.HBox([sl_out, tab])
        display(hbox)

        adjustor = cmr_widgets.ImageAdjustor(slide_show.figure, display_context=tab.children[0])
        analyzer = cmr_widgets.ROIAnalyzer(slide_show.axes[0, 0],
                                           data=self._data[self.cropped_slice],
                                           widget_contexts=tab.children[1].children)
        self.figures["data_tab"] = [slide_show.figure, analyzer.figure]

    def _glyph_projections(self):
        plt.ioff()
        figure, axes = plt.subplots(len(self._all_metrics), 3, figsize=(8, 8), squeeze=False)
        for slice_index, ax_row in enumerate(axes):
            [ax.imshow(self._data[self.cropped_slice][:, :, slice_index, 0].mean(-1), cmap="gray")
             for ax in ax_row]
            [(ax.invert_yaxis(), ax.axis("off")) for ax in ax_row]
            evecs = self._all_metrics[slice_index]['evecs'][self.cropped_slice]
            tmp_mask = self._masks[..., slice_index][self.cropped_slice]
            [cdtipy.plotting.tensors.plot_vectors_rgb(evecs[..., i], mask=tmp_mask, axis=ax_row[i],
                                                      glyph_type="ellipse")
             for i in range(3)]

        [_.set_title(t) for _, t in zip(axes[0], [f'EV{i + 1}' for i in range(3)])]
        figure.tight_layout()
        self.figures["glyph_tab"] = figure
        display(figure.canvas)
        plt.ion()

    def _metric_imshow_tab(self):
        plt.ioff()
        clear_output()
        output_box = ipywidgets.Box(children=[ipywidgets.Output(layout={"width":"80%", "overflow":"scroll"}),
                                              ipywidgets.Output(layout={"width":"20%",  "overflow":"scroll"})])
        display(output_box)

        with output_box.children[0]:
            fig, axes = cdtipy.plotting.layouts.metric_summary()
            xlims = [self._metric_limits[k] for k in ("MD", "FA", "HA", "E2A")]
            cdtipy.plotting.scalars.metric_maps(self._all_metrics, crop_percent=self._crop_percent,
                                                background_data=self._data[..., 1, :].mean(-1),
                                                axes=axes[2, :], masks=self._masks, clims=xlims,
                                                ylabel_spacing=25, slice_labels=self._slice_labels)
            cdtipy.plotting.scalars.metric_histograms(self._all_metrics, masks=self._masks.transpose(2, 0, 1),
                                                      axes=axes[:2, :], add_text=False, plot_mean_std=False,
                                                      xlims=xlims, slice_labels=self._slice_labels,
                                                      hist_kw=dict(kde=True, facecolor=(0, 0, 0, 0),
                                                                   edgecolor=(0, 0, 0, 0), stat="probability"),
                                                      )
            self.figures["imshow_tab"] = fig
            clear_output()
            display(fig.canvas)

        with output_box.children[1]:
            adjustor = cmr_widgets.ImageAdjustor(fig, axes=axes[2:].flatten())

    def _extended_hist_tab(self):
        plt.ioff()
        figure, axes = plt.subplots(2, 7, sharey="row", height_ratios=(0.2, 0.8),
                                    figsize=(16, 4), constrained_layout=True)

        keys = ("MD", "FA", "EV1", "EV2", "EV3", "HA", "E2A")
        xlims = [self._metric_limits[k] for k in keys]
        [ax.set_title(t) for ax, t in zip(axes[0], keys)]

        extended_dicts = [dict(EV1=met["evals"][..., 0], EV2=met["evals"][..., 1], EV3=met["evals"][..., 2],
                               **met) for met in self._all_metrics]
        histogram_style = dict(kde=True, facecolor=(0, 0, 0, 0), edgecolor=(0, 0, 0, 0), stat="probability")
        cdtipy.plotting.scalars.metric_histograms(extended_dicts, masks=self._masks.transpose(2, 0, 1),
                                                  keys=keys, axes=axes, add_text=True,
                                                  plot_mean_std=False,
                                                  xlims=xlims, slice_labels=self._slice_labels,
                                                  hist_kw=histogram_style, text_anchor=(0.05, 0.95))
        clear_output()
        display(figure.canvas)
        self.figures["hist_tab"] = figure

    def _local_basis_tab(self):
        plt.ioff()
        figure, axes = plt.subplots(2, 3, figsize=(10, 5))
        for slice_idx, ax_col in enumerate(axes.T):
            ax_col[0].imshow(self._data[self.cropped_slice][:, :, slice_idx, 0].mean(-1),
                             cmap='gray')
            ax_col[0].imshow(
                np.ma.masked_less(self._all_metrics[slice_idx]['TD'][self.cropped_slice], -0.))
            cdtipy.plotting.vectors.quiver_plot_inplane(
                self._all_metrics[slice_idx]['local_basis'][self.cropped_slice],
                mask=self._masks[self.cropped_slice][..., slice_idx],
                axis=ax_col[1], fov=np.array(self._data[self.cropped_slice].shape[0:2]))
        plt.ion()
        display(figure.canvas)
        self.figures["local_basis_tab"] = figure

    def _metric_analyzer_tab(self):
        sl_out = ipywidgets.VBox([ipywidgets.Output(), ipywidgets.Output()],
                                 layout={"width":"40%", "overflow":"scroll"})
        control_tab = ipywidgets.Tab([ipywidgets.HBox([ipywidgets.Output(layout={"width": "35%", "overflow": "scroll"}),
                                                ipywidgets.Output(layout={"width": "65%", "overflow": "scroll"})]),
                                      ipywidgets.Output()],
                              layout={"width":"60%", "overflow":"scroll"})
        for i, title in enumerate(["Draw", "Adjust"]):
            control_tab.set_title(i, title)
        hbox = ipywidgets.HBox([sl_out, control_tab])
        display(hbox)

        md, fa, ha, e2a = [np.stack([mets[k][self.cropped_slice] for mets in self._all_metrics], axis=-1)
                           for k in ("MD", "FA", "HA", "E2A")]
        tmp_data = np.stack([md, fa, ha, e2a], axis=-1)
        plt.ioff()
        figure, axes = plt.subplots(2, 2)
        plt.ion()
        with sl_out.children[1]:
            display(figure.canvas)

        cmaps = np.array([["viridis", "viridis"], ["jet", "seismic"]]).reshape(1, 4)
        clims = np.array([[dict(vmin=0.5e-3, vmax=2.5e-3), dict(vmin=0.1, vmax=0.7)],
                          [dict(vmin=-80, vmax=80), dict(vmin=0, vmax=90)]]).reshape(1, 4)
        slide_show = cmr_widgets.SlideShow(tmp_data, image_axes=(0, 1), col_axis=-1,
                                           cmaps=cmaps, clims=clims,
                                           slider_names=["Slice", "Metric"],
                                           figure_axes=(figure, axes.reshape(1, 4)),
                                           slider_context=sl_out.children[0],
                                           figure_context=sl_out.children[1],
                                           slider_kws=dict(orientation="horizontal"),
                                           slider_container_type=ipywidgets.VBox)
        analyzer = cmr_widgets.ROIAnalyzer(slide_show.axes[0, 0], tmp_data,
                                           widget_contexts=control_tab.children[0].children);
        adjustor = cmr_widgets.ImageAdjustor(slide_show.figure,display_context=control_tab.children[1])
        self.figures["metric_analyzer_tab"] = [slide_show.figure, analyzer.figure]
