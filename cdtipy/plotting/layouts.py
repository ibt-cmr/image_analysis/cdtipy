__all__ = ["metric_summary"]

from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import matplotlib.pyplot as plt


def metric_summary(figsize = (12, 10), height_ratios = (0.3, 0.7)
                   ) -> (plt.Figure, np.ndarray):
    """Creates a figure with 3 rows and 4 columns of subplots,
    to accommodate the metric summary plots for MD, FA, HA and E2A
    including histogram + box plots and the maps

    :param figsize: Tuple(float, float)
    :param height_ratios: Tuple(float, float)
    :return: figure, axes
    """
    figure, axes = plt.subplots(2, 4, constrained_layout=True,
                                height_ratios=height_ratios,
                                figsize=figsize, sharey="row")
    dividers = [make_axes_locatable(ax) for ax in axes[0]]
    axes_box = np.array([divider.append_axes("top", size="30%", pad=0.05) for divider in dividers])
    [figure.add_axes(ax) for ax in axes[0]]
    return figure, np.concatenate([axes_box[np.newaxis], axes], axis=0)