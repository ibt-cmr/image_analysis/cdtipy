""" Modules that contain routines to display the results of tensor-inference. The plotting methods are grouped into
submodules for scalar tensor metrics, vector plots and tensor projections with glyphs
"""
__all__ = ['scalars', 'vectors', 'tensors', 'ResultSummary', 'mask_contour_overlay']

import cdtipy.plotting.layouts
import cdtipy.plotting.scalars
import cdtipy.plotting.vectors
import cdtipy.plotting.tensors
from cdtipy.plotting._summary import ResultSummary
from cdtipy.plotting._images import mask_contour_overlay
