__all__ = ["mask_contour_overlay"]
from typing import Sequence
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np


def mask_contour_overlay(axes: Sequence[plt.Axes], binary_masks: np.ndarray,
                         contour_kwargs: dict = None):
    """Adds the contours of a binary mask as Polygon patch to the specified axes.
    The number of specified axes must match the length of the binary mask array

    :param axes: Sequence containing the axes objects to plot on. If length is equal
                to the number of masks, a one-to-one association is assumed
    :param binary_masks: ([N], X, Y) array containing the binary masks
    :param contour_kwargs: dictionary containing the keyword arguments or patches.Polygon
    """
    if contour_kwargs is None:
        contour_kwargs = {'edgecolor': (1, 0, 0, 1), 'facecolor': (1, 1, 1, 0)}

    if len(binary_masks.shape) == 2:
        binary_masks = binary_masks[np.newaxis]

    if len(binary_masks) > 1 and len(binary_masks) != len(axes):
        raise ValueError("Number of specified masks is not compatible with number of axes:"
                         f"Got {len(binary_masks)} vs {len(axes)}")
    elif len(binary_masks) == 1 and len(axes) > 0:
        binary_masks = np.repeat(binary_masks, len(axes), 0)
    print(binary_masks.shape, axes.shape)
    all_patches = [[patches.Polygon(c[:, 0], **contour_kwargs) for c in
                    cv2.findContours(m.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[0]]
                   for m in binary_masks]

    for ax, patches_per_mask in zip(axes, all_patches):
        for p in patches_per_mask:
            ax.add_patch(p)