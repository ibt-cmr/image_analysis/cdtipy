""" Common routines to plot non-scalar maps"""
__all__ = ["plot_vectors_rgb"]

from typing import Union, List

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection

import numpy as np
import tensorflow as tf


def value_vector_overlay(eigen_values: np.ndarray, eigen_vectors: np.ndarray, mask: np.ndarray,
                         fig_ax: tuple = None, glyph_type: str = 'ellipse',
                         glyphs_kwargs: dict = {}):
    """

    :param eigen_values:
    :param eigen_vectors:
    :param mask:
    :param fig_ax:
    :param glyph_type: str out of []
    :param glyphs_kwargs: dict pass through to glyph callable
    :return:
    """
    if fig_ax is None:
        f, axes = plt.subplots(1, 3)
        [(_.invert_yaxis(), _.axis('off')) for _ in axes]
        [axes[i].set_title(f'EV{i + 1}') for i in range(3)]
    else:
        f, axes = fig_ax

    [(axes[i].imshow(eigen_values, cmap='gray', vmin=0, vmax=3.e-3, origin='lower')) for i in range(3)]
    f.suptitle('EV Projection onto Slice')

    # Warning takes a bit: Eigenvector projection onto the plane
    _ = [plot_vectors_rgb(eigen_vectors[..., i], mask=mask, axis=axes[i], glyph_type=glyph_type,
                          glyphs_kwargs=glyphs_kwargs) for i in range(3)]
    return f, axes


def plot_vectors_rgb(largest_evecs: Union[tf.Tensor, np.ndarray], mask: Union[tf.Tensor, np.ndarray] = None,
                     axis: 'plt.Axes' = None, glyph_type: str = 'ellipse', glyphs_kwargs: dict = {}):
    """

    :param largest_evecs: (X, Y, 3, 3)
    :param mask:
    :param axis:
    :param glyph_type: str out of []
    :param glyphs_kwargs: dict pass through to glyph callable
    :return:
    """
    if mask is None:
        mask = tf.ones(largest_evecs.shape[0:2])

    if axis is None:
        f, axis = plt.subplots(1, 1)

    glyph_function = _glyph_type_map.get(glyph_type.lower(), 'ellipse')
    glyphs: List[patches.Patch] = glyph_function(largest_evecs, mask, **glyphs_kwargs)
    patch_collection = PatchCollection(glyphs, match_original=True)
    axis.add_collection(patch_collection)
    return axis


def get_rgb_glyphs(largest_evs: Union[np.ndarray, tf.Tensor], mask: Union[np.ndarray, tf.Tensor],
                   stretch: float = 1., linewidth: float = 0.1) -> List[patches.Ellipse]:
    """

    :param largest_evs: (X, Y, 3)
    :param mask: (X, Y)
    :param stretch: float - determines length of glyphs
    :param linewidth: float - determines width of glyphs
    :return:
    """
    xind, yind = tf.meshgrid(*[tf.range(i, dtype=tf.float32) for i in mask.shape[::-1]], indexing='xy')
    xind, yind, largest_evs = [tf.gather_nd(_, tf.where(mask > 0.01)) for _ in (xind, yind, largest_evs)]

    x_project = stretch * largest_evs[..., 0]
    y_project = stretch * largest_evs[..., 1]

    # calculate polygon colors
    polygon_rgb = [(r, g, b, 1.) for (r, g, b) in np.abs(largest_evs)]

    # calculate polygon coordinates
    c = y_project / x_project
    t1 = tf.sqrt(linewidth**2 / (1 + c**2))
    t2 = c * t1

    polycoords_y = np.array([yind - y_project/2. - t2, yind + y_project/2. - t2,
                             yind + y_project/2. + t2, yind - y_project/2. + t2]).reshape(4, -1)
    polycoords_x = np.array([xind - x_project/2. + t1, xind + x_project/2. + t1,
                             xind + x_project/2. - t1, xind - x_project/2. - t1]).reshape(4, -1)

    polygon_coords = np.stack((polycoords_x.transpose([1, 0]), polycoords_y.transpose([1, 0])), axis=2)
    plot_patches = [patches.Polygon(p, facecolor=c) for p, c in zip(polygon_coords, polygon_rgb)]
    return plot_patches


def get_rgb_ellipses(largest_evs: Union[np.ndarray, tf.Tensor], mask: Union[np.ndarray, tf.Tensor],
                     stretch: float = 1.) -> List[patches.Ellipse]:
    """

    :param largest_evs: (X, Y, 3)
    :param mask: (X, Y)
    :param stretch: float - determines length of glyphs
    :return:
    """
    xind, yind = tf.meshgrid(*[tf.range(i, dtype=tf.float32) for i in mask.shape[::-1]], indexing='xy')
    xind, yind, largest_evs = [tf.gather_nd(_, tf.where(mask > 0.01)) for _ in (xind, yind, largest_evs)]

    x_project, y_project, z_project = [largest_evs[..., i] for i in range(3)]
    polygon_rgb = [(r, g, b, 1.) for (r, g, b) in np.abs(largest_evs)]

    min_length = 1/3 * stretch

    width = tf.ones_like(x_project) * min_length + 2 * min_length * (1 - tf.abs(z_project))
    height = tf.ones_like(x_project) * min_length
    angle = tf.math.angle(tf.complex(x_project, y_project))
    xy = tf.stack([xind, yind], axis=-1)

    plot_patches = [patches.Ellipse(c, w, h, a, facecolor=color) for c, w, h, a, color in
                    zip(xy, width, height, np.rad2deg(angle), polygon_rgb)]
    return plot_patches


_glyph_type_map = {'lines': get_rgb_glyphs,
                   'ellipse':  get_rgb_ellipses}
