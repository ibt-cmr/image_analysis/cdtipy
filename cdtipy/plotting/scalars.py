""" Common routines to plot scalar maps"""
__all__ = ["metric_maps", "metric_histograms"]

from typing import Tuple, List, Dict
import numpy as np
import math
import seaborn as sns
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable


def metric_maps(metrics: List[Dict], keys: Tuple[str] = ("MD", "FA", "HA", "E2A"), background_data: np.ndarray = None,
                masks: np.ndarray = None, axes: plt.Axes = None,
                crop_percent: Tuple[float, float, float, float] = (0, 1, 0, 1),
                figure_kwargs: Dict = None,
                cmaps: Tuple[str] = ("viridis", "viridis", "rainbow", "seismic"),
                clims: Tuple[Tuple[float, float], ...] = ((0.9e-3, 1.7e-3), (0.25, 0.5), (-80, 80), (0, 90)),
                slice_labels: Tuple[str, ...] = ("Apical", "Mid", "Basal"),
                ylabel_spacing: int = 10,
                ) -> plt.Figure:
    """

    :param metrics: List[Dict[str: np.ndarray]] each map is of shape (X, Y)
    :param keys: list of strings to index the metrics dictionaries
    :param background_data:
    :param masks: np.ndarray (len(metrics), X, Y)
    :param axes:
    :param crop_percent:
    :param figure_kwargs:
    :param cmaps:
    :param clims:
    :param slice_labels:
    :return:
    """
    if axes is not None:
        # if len(metrics) != axes.shape[0]: raise ValueError("Axes rows don't match the number of slices")
        if len(keys) != axes.shape[0]: raise ValueError("Axes cols don't match the number of keys")
        figure = axes[0].figure
    else:
        if figure_kwargs is None: figure_kwargs = {}
        figure, axes = plt.subplots(1, len(keys), **figure_kwargs)
    [(ax.set_xticks([]), ax.set_yticks([])) for ax in axes]

    image_shape = np.repeat(metrics[0][keys[0]].shape, 2)
    cropped_indices = (np.array(crop_percent) * image_shape).astype(int)
    cropped_slice = (slice(*cropped_indices[:2], 1), slice(*cropped_indices[2:], 1))

    if masks is None:
        masks = np.ones([len(metrics), *metrics[0][keys[0]].shape])
    concatenated_masks = np.concatenate([m[cropped_slice] for m in masks.transpose(2, 0, 1)], axis=0)
    concatenated_masks[np.where(concatenated_masks < 1)] = np.nan
    if background_data is not None:
        [ax.imshow(np.concatenate([bg[cropped_slice] for bg in background_data.transpose(2, 0, 1)], axis=0), cmap="gray")
         for ax in axes]

    image_artists = []
    for ax_col, key, cm, cl in zip(axes, keys, cmaps, clims):
        temp = np.concatenate([met[key][cropped_slice] for met in metrics], axis=0)
        art = ax_col.imshow(temp * concatenated_masks, cmap=cm, vmin=cl[0], vmax=cl[1])
        image_artists.append(art)
    axes[0].set_ylabel((" " * ylabel_spacing).join(slice_labels[:len(metrics)][::-1]))

    cbars = [figure.colorbar(art, ax=ax, orientation="horizontal", location="bottom", shrink=0.9, pad=0.01, label=k)
             for art, ax, k in zip(image_artists, axes, keys)]
    [cb.ax.ticklabel_format(style="sci", scilimits=(-2, 2)) for cb in cbars]
    return figure


def metric_histograms(metrics: List[Dict],
                      masks: np.ndarray = None,
                      keys: Tuple[str] = ("MD", "FA", "HA", "E2A"),
                      slice_labels: Tuple[str, ...] = ("Apical", "Mid", "Basal"),
                      xlims: Tuple[Tuple[float, float], ...] = ((0.9e-3, 1.7e-3), (0.25, 0.5), (-80, 80), (0, 90)),
                      bins: np.ndarray = None,
                      xlabels: Tuple[str, ...] = ('$mm^2/s$', ' a.u ', '$\degree$', '$\degree$'),
                      hist_kw: dict = None, axes: plt.Axes = None,
                      plot_mean_std: bool = True, add_text: bool = True, box_offset: float = 0,
                      colors: Tuple[str, ...] = None, format_axes: bool = True,
                      text_anchor: Tuple[float, float] = (0.05, 0.1),
                      fontsize: float = 8,
                      ):
    """Plots a set of histograms-density plot for MD, FA, EV1-3, HA and E2A in 7 subplots in a
    single figure. Above each histogram figure a horizontal box-plot corresponding to the
    distribution below. Also mean+- std is plotted as vertical lines in each histogram plot as
    well as a text specifying mean+-std

    :param metrics: List[Dict[str: np.ndarray]] each map is of shape (X, Y)
    :param masks: np.ndarray (len(metrics), X, Y)
    :param keys: list of strings to index the metrics dictionaries
    :param slice_labels: list of labels, same len as metrics
    :param xlims: tuples to determine the histogram boundaries, one for each keay
    :param bins: bins to use for histograms, one for each key
    :param xlabels: prefered units for each column
    :param hist_kw: direct pass through to sns.histplot
    :param axes: array of axes, must have shape (2, len(keys))
    :param plot_mean_std: if true plots vertical lines for mean and histogram
    :param add_text:
    :param box_offset:
    :param colors:
    :param format_axes:
    :param text_anchor:
    :return:
    """
    if axes is None:
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        figure, axes_hist = plt.subplots(nrows=1, ncols=len(keys), figsize=(len(keys) * 3, 5))
        dividers = [make_axes_locatable(ax) for ax in axes_hist]
        axes_box = [divider.append_axes("top", size="30%", pad=0.01) for divider in dividers]
        [figure.add_axes(ax) for ax in axes_box]
        all_axes = (axes_box, axes_hist)
    else:
        figure = axes[0, 0].get_figure()
        if len(keys) != axes.shape[1]: raise ValueError("Axes cols don't match the number of keys")
        all_axes = axes
    axes_box, axes_hist = all_axes

    if bins is None:
        bins = [np.linspace(l, r, 40) for (l, r) in xlims]

    hist_kw_default = dict(kde=True, stat='probability')
    if hist_kw is None:
        hist_kw = {}
    hist_kw_default.update(hist_kw)

    if colors is None:
        colors = [f"C{i}" for i in range(len(metrics))]

    if masks is None:
        masks = [np.ones_like(met[keys[0]]) for met in metrics]

    boxprops = dict(linestyle='-', linewidth=1, color='black')
    flierprops = dict(marker='.', markerfacecolor='k', markersize=0, markeredgecolor='none')
    medianprops = dict(linestyle='-', linewidth=1, color='black')

    text_strings = []
    for idx, met, mask, color in zip(range(len(metrics)), metrics, masks, colors):
        mask_indices = np.where(mask > 0)
        flat_values = [met[k][mask_indices] for k in keys]
        flat_values = [fl[np.isfinite(fl)] for fl in flat_values]

        for ax_b, ax_h, fl, bins_ in zip(axes_box, axes_hist, flat_values, bins):
            sns.histplot(data=fl, ax=ax_h, bins=bins_, color=color, **hist_kw_default)

            patch = ax_b.boxplot(fl, vert=False, positions=[box_offset - idx / 2, ], widths=0.5,
                                 patch_artist=True, boxprops=boxprops, flierprops=flierprops,
                                 medianprops=medianprops)
            patch["boxes"][0].set_facecolor(color)
            patch["boxes"][0].set_alpha(0.7)

            mean, std = fl.mean(), fl.std()
            if plot_mean_std:
                ax_h.axvline(mean, ls='-', c=color, lw=1)
                ax_h.axvline(mean - std, ls='--', lw=0.75, c=color)
                ax_h.axvline(mean + std, ls='--', lw=0.75, c=color)

            # Add text for mu and sigma
            xr = ax_h.get_xlim()
            mean_str = f"{mean * 10. ** (-math.floor(math.log10(xr[-1]))): 1.2f}"
            std_str = f"{std * 10. ** (-math.floor(math.log10(xr[-1]))): 1.2f}"
            order_of_magnitude_str = str((math.floor(math.log10(xr[-1]))))
            text_strings.append(f"$\mu\pm\sigma$ ({mean_str} $\pm$ {std_str}) $\cdot 10^{{{order_of_magnitude_str}}}$")

    if format_axes:
        [ax.set_xlabel(xlbl) for ax, xlbl in zip(axes_hist, xlabels)]
        [ax.set_xlim(xl) for ax, xl in zip(axes_hist, xlims)]
        [ax.set_xlim(xl) for ax, xl in zip(axes_box, xlims)]
        [ax.set_ylabel("") for ax in axes_hist]
        [ax.grid(alpha=0.5) for ax in axes_hist]
        [ax.grid(alpha=0.5) for ax in axes_box]
        [(ax.set_xticklabels([]), ax.set_yticklabels([])) for ax in axes_box]
        [ax.ticklabel_format(style='sci', axis='x', scilimits=(-3, 3), useMathText=True)
         for ax in axes_hist]

    if add_text:
        text_strings = np.array(text_strings).reshape(len(metrics), len(keys))
        for idx, ax_h in enumerate(axes_hist):
            xr, yr = ax_h.get_xlim(), ax_h.get_ylim()
            text_anchor_coords = [xr[0] + (xr[-1] - xr[0]) * text_anchor[0], yr[-1] * text_anchor[1]]
            tmp_str = "\n".join(text_strings[:, idx])
            ax_h.text(*text_anchor_coords, tmp_str, fontsize=fontsize, bbox=dict(facecolor='white', alpha=0.6),
                      verticalalignment ="top")
    legends = [ax.legend([plt.Line2D([0], [0], color=c) for c in colors], slice_labels, fontsize=8) for ax in axes_hist]
    return axes, legends
