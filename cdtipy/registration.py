__all__ = ["call_ptv_registration"]

import os
import datetime
import itertools
from tqdm.notebook import tqdm
from IPython.display import clear_output

try:
    import matlab.engine
    import matlab
except ModuleNotFoundError:
    import warnings
    warnings.warn("No matlab installation found!")

import numpy as np


def call_ptv_registration(data: np.ndarray, eng=None, delete_temporary_files: bool = True):
    """ Helper function to call matlab script per slice and b value

    :param data: np.ndarray of shape (X, Y, #slice, #b, #avg)
    :param eng: matlab enginge instance
    :return: (np.ndarray, np.ndarray) registered_data, displacements
    """

    # start matlab engine and add required code to the MATLAB-PATH
    if eng is None:
        eng = matlab.engine.start_matlab()
        matlab_code_path = [os.path.abspath(os.path.dirname(__file__)) + "/matlab_code", ]
        for p in matlab_code_path:
            eng.addpath(eng.genpath(p))

    temp_time_stamp = f"registration_tmp_{datetime.datetime.now().strftime('%m%d%Y_%H.%M.%S')}"
    os.makedirs(temp_time_stamp, exist_ok=False)

    # 1. Register all averages per b_idx
    if data.shape[4] > 1:
        for slice_index, b_idx in tqdm(itertools.product(range(data.shape[2]), range(data.shape[3])),
                                       total=np.prod(data.shape[2:4]), desc="Registering Averages per bval: "):
            matlab_input = matlab.double((data[:, :, slice_index, b_idx:b_idx + 1]).tolist(),
                                         is_complex=True)
            matlab_mask = matlab.double(np.ones(data.shape[0:2], dtype=np.int64).tolist())
            registered_slice, displacements = eng.register_single_slice(matlab_input, matlab_mask,
                                                                        nargout=2)
            np.savez(f"{temp_time_stamp}/temp_result_{slice_index}_{b_idx :02}.npz",
                     registered_slice=registered_slice,
                     displacements=displacements)
        print("IN registration", np.array(registered_slice).shape)
    clear_output()

    
    # Stack data to (x, y, s, 1, b, avg)
    all_data = np.stack([np.stack(
    [np.load(f"{temp_time_stamp}/temp_result_{slice_index}_{b_idx :02}.npz")["registered_slice"]
     for b_idx in range(data.shape[3])], axis=-2) for slice_index in range(data.shape[2])],
                    axis=2)
    print("All Data shape: ", all_data.shape)
    
    # 2. Register the averaged data over b-values
    combined_displacements = []
    # registered_slices = []
    for slice_index in tqdm(range(data.shape[2]), desc="Register per averaged b-value:"):
        averaged_data = np.abs(all_data[:, :, slice_index]).mean(-1)
        print("AVg Data shape: ", averaged_data.shape)
        
        averaged_data /= np.abs(averaged_data).max(axis=(0, 1), keepdims=True)
        matlab_input = matlab.double(averaged_data.tolist(), is_complex=True)
        matlab_mask = matlab.double(np.ones(data.shape[0:2], dtype=np.int64).tolist())
        registered_slice, displacements = eng.register_single_slice(matlab_input, matlab_mask,
                                                                    nargout=2)
        combined_displacements.append(displacements)
        # registered_slices.append(registered_slice)
    combined_displacements = np.stack(combined_displacements, axis=2)
    clear_output()
    
    # 3. Apply displacements to all images
    results = []
    for slice_index, b_idx in tqdm(itertools.product(range(data.shape[2]), range(data.shape[3])),
                                   total=np.prod(data.shape[2:4]), desc="Apply displacements: "):
        mdatr = matlab.double(np.real(all_data[:, :, slice_index, :, b_idx]).tolist(),
                              is_complex=False)
        mdati = matlab.double(np.imag(all_data[:, :, slice_index, :, b_idx]).tolist(),
                              is_complex=False)
        mdisp = matlab.double(
            np.repeat(combined_displacements[:, :, slice_index, :, :, b_idx:b_idx+1],
                      axis=-1, repeats=data.shape[-1]).tolist(),
            is_complex=False)
        imdef_real = np.array(eng.ptv_deform(mdatr, mdisp, 1, nargout=1))
        imdef_imag = np.array(eng.ptv_deform(mdati, mdisp, 1, nargout=1))
        results.append(imdef_real + 1j * imdef_imag)

    results = np.stack(results, axis=-2).reshape(data.shape)

    if delete_temporary_files:
        import shutil
        shutil.rmtree(f"{temp_time_stamp}")
    return results #, registered_slices, all_data, combined_displacements