__all__ = ["load_parrec_dataset", "bvec_mps_from_header"]

from parrec.parread import Parread
from parrec.recread import Recread
import numpy as np
from scipy.spatial.transform import Rotation


def load_parrec_dataset(parfile_path: str):
    """ Loads a dataset consisting out of a .par and a .rec file in the phillips specific format.
    Returns the meta-information of the par-file as dictionary, and a squeeze
    :param parfile_path:
    :return: (dict, np.ndarray, np.ndarray) Image-Info, Data, b-vectors
    """
    reader = Parread(parfile_path)
    par_dict = reader.read()
    par_dict["ImageInformation"] = {k: [img[k] for img in par_dict["ImageInformation"]]
                                    for k in par_dict["ImageInformation"][0].keys()}

    reader = Recread(parfile_path.replace(".par", ".rec"))
    rec_data = reader.read()
    rec_data = np.abs(np.squeeze(rec_data))

    b_vecs_slice = bvec_mps_from_header(par_dict)

    return par_dict, rec_data, b_vecs_slice


def bvec_mps_from_header(parfile_dict: dict):
    """ Extracts directions from .par file information and returns sqrt-scaled
    b_vectors in slice coordinate system
    :param parfile_dict:
    :return: (N, 3) directions scaled with sqrt(b)
    """
    b_vectors_raw = _load_raw_bvectors(parfile_dict)
    b_values = np.round(np.einsum('ni, ni -> n', b_vectors_raw, b_vectors_raw), decimals=3)[:, np.newaxis]
    directions_raw = np.divide(b_vectors_raw, np.sqrt(b_values), where=b_values > 0)

    # Todo: Why - angulation? - Legacy confusion...
    mid_slice_angulation = - np.array(parfile_dict['AngulationMidslice']).flatten()
    slice_orientation = int(np.array(parfile_dict['ImageInformation']['SliceOrientation'])[0])
    slice_orientation = ['tra', 'sag', 'cor'][slice_orientation-1]
    ap, fh, rl = np.deg2rad(mid_slice_angulation)
    rot_m = _rotation_matrix_raf(ap, fh, rl)
    directions_xyz = np.einsum('ij, nj -> ni', rot_m, directions_raw)
    directions_xyz = np.array([-directions_xyz[:, 0], directions_xyz[:, 2], -directions_xyz[:, 1]]).T
    directions_ixyz = np.einsum('ij, nj -> ni', _xyz_to_ixiyiz(slice_orientation), directions_xyz)
    return directions_ixyz * np.sqrt(b_values)


def _load_raw_bvectors(parfile_dict: dict):
    """ Extracts directions from .par file information and returns sqrt-scaled
    b_vectors in patient coordinates system
    :param parfile_dict:
    :return: (N, 3) directions scaled with sqrt(b)
    """
    slice_indices = np.array(parfile_dict['ImageInformation']['SliceNumber'], dtype=np.int32).flatten()
    b_values = np.array([parfile_dict['ImageInformation']['DiffusionBFactor']]).flatten()
    n_slices = int(parfile_dict['MaxNumberOfSlicesLocations'])

    directions_raw = np.array(parfile_dict['ImageInformation']['Diffusion'])

    if np.all(np.equal(slice_indices[0:n_slices], np.arange(1, n_slices + 1))):
        b_values_unique = b_values[::n_slices]
        directions_unique = directions_raw[::n_slices]
    elif all(slice_indices[0:n_slices] == 1):
        b_values_unique = b_values[0:n_slices]
        directions_unique = directions_raw[0:n_slices]

    b_values_unique = _bugfix_bvalues(b_values_unique)  # noqa
    b_vectors = np.einsum('ni, n -> ni', directions_unique, np.sqrt(b_values_unique))  # noqa
    return b_vectors


def _bugfix_bvalues(b_values: np.ndarray):
    """ According to C. Stoeck sometimes the b values for the (3x100 - 9x450)
    are stored incorrectly as 12x450...
    """
    if b_values.shape[0] == 13 and all([i == 450 for i in b_values[1:4]]):
        b_values[1:4] = 100
    return b_values


def _rotation_matrix_raf(ap_angle, fh_angle, rl_angle):
    """ Calculates the rotation matrix that transforms vectors from AP-FH-RL
     coordinates to mps coordinates. Angles need to be specified in radians
    """
    ap_rot = Rotation.from_rotvec(ap_angle * np.array([1., 0., 0.]))
    fh_rot = Rotation.from_rotvec(fh_angle * np.array([0., 1., 0.]))
    rl_rot = Rotation.from_rotvec(rl_angle * np.array([0., 0., 1.]))
    return fh_rot.as_matrix() @ ap_rot.as_matrix() @ rl_rot.as_matrix()


def _pat_to_xyz(directions_pat: np.ndarray) -> np.ndarray:
    """ AP <-> -X, RL <-> Y, FH <-> -Z
    :param directions_pat: (N, 3)
    :return: (N, 3)
    """
    directions_xyz = directions_pat[:, (0, 2, 1)]
    directions_xyz[:, (0, 2)] *= -1.
    return directions_xyz


def _xyz_to_ixiyiz(orientation: str):
    """ Calculates matrix depending on slice orientation according to following
     coordinate transformations:
      transverse:  (Y, X, Z) -> (iX, -iY, -iZ)
      saggital: (X, Z, Y) -> (-iX, iZ, -iY)
      coronal: (Y, Z, X) <-> (iX, iY, -iZ)

    :param orientation: in [tra, sag, cor] not case sensitive
    :return:
    """
    ori = orientation.lower()

    if ori not in ['tra', 'sag', 'cor']:
        raise ValueError(f'Unrecognised slice orientation {orientation}'
                         f' not in [tra, sag, cor]')

    if ori == 'tra':
        permutation, signs = (1, 0, 2), [1, -1, -1]
    elif ori == 'sag':
        permutation, signs = (0, 2, 1), [-1, 1, -1]
    elif ori == 'cor':
        permutation, signs = (1, 2, 0), [1, 1, -1]

    identity_m = np.eye(3, 3, dtype=np.float32)
    signs = np.array(signs, dtype=np.float32)[..., np.newaxis]  # noqa
    transformation = identity_m[permutation, :] * signs  # noqa
    return transformation