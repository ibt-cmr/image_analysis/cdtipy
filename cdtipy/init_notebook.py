import os
import sys
from shutil import copy

if __name__ == "__main__":

    # Get path
    if '--location' in sys.argv:
        path_index = sys.argv.index('--location') + 1
        path = sys.argv[path_index]
        sys.argv.pop(path_index-1)
        sys.argv.pop(path_index-1)
    else:
        path = '.'

    absolute_location = os.path.abspath(path)
    resource_path = os.path.abspath(os.path.dirname(__file__)) + '/AnalyzeData.ipynb'
    copy(src=resource_path, dst=absolute_location)
