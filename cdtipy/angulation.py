__all__ = ["angulation_set", "helix_angle", "transverse_angle", "sheetlet_angle"]

import tensorflow as tf
import numpy as np
from scipy.spatial.transform import Rotation

import cdtipy.coordinates


def angulation_set(eigen_vectors: np.ndarray, mask: np.ndarray, field_of_view: np.ndarray, fill_value=np.nan, homogeneous_handedness: bool = True):
    """ Computes Helix, Transverse and absolute sheetlet angle. Transmural depth map and local coordinate basis
    per pixel (radial, circumferential, longitudinal) is return along with the angle maps in a dictionary.

    :param eigen_vectors: (X, Y, 3, 3)
    :param mask: (X, Y)
    :param field_of_view: (2, )
    :param fill_value: background value of angle maps
    :return: dict('HA', 'TA', 'E2A', 'TD', 'local_basis')
    """
    polar_coords, cartesian_coords = cdtipy.coordinates.get_mask_centered_coordinates(mask=mask, fov=field_of_view)
    transmural_depth_map = cdtipy.coordinates.compute_transmural_position(pixel_coordinates=cartesian_coords, mask=mask)
    local_basis = cdtipy.coordinates.compute_local_basis_2d(cartesian_coords, mask=mask, homogeneous_handedness=homogeneous_handedness).numpy()

    ev1, ev2 = [eigen_vectors[..., j].astype(np.float32) for j in range(2)]
    ev1_masked, ev2_masked, local_basis_masked = [_[mask > 0] for _ in [ev1, ev2, local_basis.astype(np.float32)]]

    ha = cdtipy.angulation.helix_angle(ev1_masked, local_basis_masked)
    ta = cdtipy.angulation.transverse_angle(ev1_masked, local_basis_masked)
    e2a = tf.abs(cdtipy.angulation.sheetlet_angle(ev1_masked, ev2_masked, local_basis_masked))

    ha_map, ta_map, e2a_map = [np.full_like(transmural_depth_map, fill_value=fill_value) for _ in range(3)]
    ha_map[mask > 0], ta_map[mask > 0], e2a_map[mask > 0] = ha, ta, e2a
    angulation_results = {'HA': np.array(ha_map), 'TA': np.array(ta_map),
                          'E2A': np.array(e2a_map), 'TD': np.array(transmural_depth_map),
                          'local_basis': np.array(local_basis)}
    return angulation_results


def transverse_angle(ev1_flat: tf.Tensor, masked_local_basis: tf.Tensor) -> tf.Tensor:
    """ Calculates transverse angle for eigen-vectors for the given local basis over a flat tensors.

    This code was translated from Matlab based DTI GUI (@IBT) with courtesy of Robbert van Gorkum.

    :param ev1_flat: (-1, 3) Where the innermost axis contains the eigenvector corresponding to the largest
                                        eigenvalue of diffusion tensors.
    :param masked_local_basis: (-1 ..., 3, 3) Where the columns of the innermost matrices contain the local basis
                                        vectors (radial, circular, longitudinal).

    :return: -> np.ndarray (...) in degree
    """
    # slice local basis vector (bv) for more comprehensible code
    radial_bv, circumferential_bv, long_bv = [masked_local_basis[..., i] for i in range(3)]

    projection_trans_plane = ev1_flat - long_bv * tf.einsum('ni, ni -> n', ev1_flat, long_bv)[:, tf.newaxis]

    signum = -1. * (tf.sign(tf.einsum('ni, ni -> n', circumferential_bv, projection_trans_plane))
                    * tf.sign(tf.einsum('ni, ni -> n', radial_bv, projection_trans_plane)))
    signum = tf.where(signum == 0., tf.ones_like(signum), signum)

    # Assemble parts to transverse angle
    temp = tf.linalg.norm(tf.linalg.cross(projection_trans_plane, circumferential_bv), axis=-1)
    angle_values = tf.math.asin(tf.clip_by_value(temp, -1., 1.)) * tf.constant(180. / np.pi) * signum

    return angle_values


def helix_angle(ev1_flat: tf.Tensor, masked_local_basis: tf.Tensor) -> tf.Tensor:
    """ Calculates helix angle for eigen-vectors for the given local basis over a flat tensors.

    This code was translated from Matlab based DTI GUI (@IBT) with courtesy of Robbert van Gorkum.

    :param ev1_flat: (-1, 3) Where the innermost axis contains the eigenvector corresponding to the largest
                                        eigenvalue of diffusion tensors.
    :param masked_local_basis: (-1, 3, 3) Where the columns of the innermost matrices contain the local basis
                                        vectors (radial, circular, longitudinal).
    :return: -> tf.Tensor (-1 ) in degree
    """
    # slice local basis vector (bv) for more comprehensible code
    radial_bv, circumferential_bv, longitudinal_bv = [masked_local_basis[..., i] for i in range(3)]
    radial_projection = tf.einsum('ni, ni -> n', ev1_flat, radial_bv)[:, tf.newaxis]
    projection_epi_plane = ev1_flat - radial_bv * radial_projection

    circ_projection = tf.einsum('ni, ni -> n', circumferential_bv, projection_epi_plane)
    long_projection = tf.einsum('ni, ni -> n', longitudinal_bv, projection_epi_plane)

    _ = - 1 * (tf.sign(circ_projection) * tf.sign(long_projection))
    signum = tf.where(tf.sign(circ_projection) == 0, tf.ones_like(_), _)

    # Assemble parts to Helix angle
    temp = tf.linalg.norm(tf.linalg.cross(circumferential_bv, projection_epi_plane), axis=-1)
    angle_values = tf.math.asin(tf.clip_by_value(temp, -1., 1.)) * tf.constant(180. / np.pi) * signum
    return angle_values


def sheetlet_angle(ev1_flat: tf.Tensor, ev2_flat: tf.Tensor, masked_local_basis: tf.Tensor):
    """
    :param ev1_flat: (-1, 3) Where the innermost axis contains the eigenvector corresponding to the largest
                                        eigenvalue of diffusion tensors.
    :param ev2_flat: (-1, 3) Where the innermost axis contains the eigenvector corresponding to the second largest
                                        eigenvalue of diffusion tensors.
    :param masked_local_basis: (-1, 3, 3) Where the columns of the innermost matrices contain the local basis
                                        vectors (radial, circular, longitudinal).
    :return: -> tf.Tensor (-1 ) in degree"""
    radial_bv, circumferential_bv, longitudinal_bv = [masked_local_basis[..., i] for i in range(3)]
    eig_1_epi_plane = ev1_flat - tf.einsum('...i, ...i -> ...', ev1_flat, radial_bv)[:, tf.newaxis] * radial_bv
    fibre_direction_proj = eig_1_epi_plane / tf.linalg.norm(eig_1_epi_plane, axis=-1, keepdims=True)
    cross_fiber_dir = tf.linalg.cross(fibre_direction_proj, radial_bv)
    cross_fiber_dir = cross_fiber_dir / tf.linalg.norm(cross_fiber_dir, axis=-1, keepdims=True)

    proj_e2 = ev2_flat - fibre_direction_proj * tf.einsum('...i, ...i -> ...', fibre_direction_proj, ev2_flat)[:, tf.newaxis]
    proj_e2 = proj_e2 / tf.linalg.norm(proj_e2, axis=-1, keepdims=True)

    projection_temp = tf.einsum('...i, ...i -> ...', cross_fiber_dir, proj_e2)
    rot_angles = tf.math.acos(tf.minimum(projection_temp, tf.ones_like(projection_temp))) / tf.constant(np.pi) * 180

    # TODO: Catch case of parallel/antiparallel vectors in cross product leading to 0-norm
    rot_vec = tf.linalg.cross(cross_fiber_dir, proj_e2)
    normed_rot_vec = rot_vec / tf.linalg.norm(rot_vec, axis=-1, keepdims=True)

    sheetlet_angles = tf.sign(tf.einsum('...i, ...i -> ...', normed_rot_vec, fibre_direction_proj)) * rot_angles
    sheetlet_angles = tf.where(sheetlet_angles > 90., sheetlet_angles - 180., sheetlet_angles)
    sheetlet_angles = tf.where(sheetlet_angles < -90., sheetlet_angles + 180., sheetlet_angles)
    return sheetlet_angles


def sector_angle():
    pass
