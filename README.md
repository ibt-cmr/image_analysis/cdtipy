# cDTIpy
---
Toolbox for processing **c**ardiac **D**iffusion-**T**ensor-**I**maging data with **py**thon.

### Included Functionality:
---
- Load data from .par/.rec files in addition to .mat and .npy files
- Inferring Diffusion Tensors from Data
- Evaluating common Tensor-metrics
- Evaluating angulation for slices
- Drawing Masks
- Plot data within regions of interest


### Requirements
---
The intended use is within a jupyter-notebook. To use all features as intended it is recommended to use a ![jupyter-lab](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html) installation >= 3.0.1. Furthermore, the ![widget extension](https://ipywidgets.readthedocs.io/en/stable/user_install.html) needs to be installed to display the plotting widgets correctly.    
As loading the .par/.rec files is done using MATLAB legacy code the used python kernel needs a registered matlab ![installation](https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html). Since this package uses tensor-flow >2.3 the python installation should be on version >= 3.8 and therefore compatible matlab versions are > R2020b.

To use registration, the please download the pTV-registration tool box: https://gitlab.ethz.ch/valeryv/ptvreg

### Getting Started
---

To install the python package from the ![registry](https://git.ee.ethz.ch/jweine/cdtipy/-/packages), (after choosing a version) either install with pip following the instructions or download the .whl and install locally with pip.     

 A default work-flow is given as a template inside the package. To copy the template to a directory containing the data to process type:

```shell
    python -m cdtipy.init_notebook --location <path/to/data>
```


