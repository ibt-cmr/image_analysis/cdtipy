import unittest

import tensorflow as tf
import numpy as np

import cdtipy.angulation


class TestAngulation(unittest.TestCase):

    def test_helix_angle(self):

        radial, circumferential, longitudinal = [-1., 0., 0.], [0., -1., 0.], [0., 0., 1.]
        input_tensors = np.array([[0., 0., 1.], [0., 1., 1.], [0., 1., -1.]], dtype=np.float32)
        ref_angles = np.array([90., 45.])

        radial, circumferential, longitudinal = [0., 1., 0.], [0., -1., 0.], [0., 0., 1.]
        input_tensors = np.array([[0., 0., 1.], [0., 1., 1.], [0., 1., -1.]], dtype=np.float32)
        ref_angles = np.array([90., 45.])

        local_basis = np.array([radial, circumferential, longitudinal], dtype=np.float32)
        a = input_tensors / np.linalg.norm(input_tensors, axis=-1, keepdims=True)
        b = np.tile(local_basis[np.newaxis], [a.shape[0], 1, 1])
        angles = cdtipy.angulation.helix_angle(ev1_flat=tf.constant(a), masked_local_basis=tf.constant(b))

        print(angles)



