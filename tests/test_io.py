import unittest

from cdtipy.io.parse import parread
import cdtipy.io.bvectors as b_io


class TestBvectors(unittest.TestCase):
    def test_load_bvectors(self):
        h = parread('D:/Data/Stoeck_Invivo/recon/st_15012016_1208010_21_2_wipdti_3slclearV4_not_registered.par')
        b_vecs_pat = b_io.pat_from_header(h)
        b_vecs_slice = b_io.slice_from_header(h)
