import unittest
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from cdtipy.plotting import tensors


class TestPlotMaps(unittest.TestCase):

    def test_plot_vector_rgb(self):
        rnd_trace = tf.random.uniform(shape=(10, 10, 3), minval=0.1, maxval=3.)
        tensors = tf.einsum('xyi, xyj -> xyij', rnd_trace, rnd_trace)
        f, a = plt.subplots(1, 1)
        a.imshow(np.ones([10, 10]))
        tensors.plot_vectors_rgb(tensors, mask=None, axis=a)
        plt.show()
