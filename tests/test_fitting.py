import unittest
import numpy as np

from cdtipy.fitting import LSQLinear


class TestLSQLinear(unittest.TestCase):
    def test_sanity_check(self):
        """ Samples random diagonal tensors with positive eigenvalues and s0 values, evaluates diffusion equation and
        uses linear least squares to fit the parameters to the signal.
        Asserts that the parameters are all close to the real value for the noise-less case!.
        """
        n_pixel = 49
        s0 = np.random.uniform(0.1, 2., (n_pixel,))
        # d_diff = 0.0013 * (np.array([1., 1., 1., 0, 0, 0], dtype=np.float64).reshape((1, 6)), [n_pixel, 1])
        d_diff = 0.0013 * np.concatenate([[[*np.random.uniform(0., 1., (3,)), 0., 0., 0.], ] for _ in range(n_pixel)])
        b_vectors = np.concatenate([np.diag([1., 1., 1.]) * np.sqrt(i) for i in [100., 300., 400., 600.]])
        initial_values = (s0, d_diff)

        bx, by, bz = b_vectors.T
        flattened_b_vectors = np.array([bx ** 2, by ** 2, bz ** 2, 2 * bx * by, 2 * bx * bz, 2 * by * bz])
        exponents = np.einsum("jb, pj -> pb", flattened_b_vectors, d_diff)
        signal = s0.reshape(-1, 1) * np.exp(-exponents)
        signal = signal.reshape([7, 7, -1, 1])

        reg = LSQLinear(signal, b_vectors)
        reg.fit()
        a1, a2 = reg.get_result()

        self.assertTrue(np.allclose(a2, reg._flat_tensor_to_matrix(d_diff)))
        self.assertTrue(np.allclose(a1.flatten(), s0.flatten()))


if __name__ == '__main__':
    unittest.main()
